/*==================================================================================================
Copyright © 2015-2018 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license.
==================================================================================================*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <atlbase.h>
#include <mbnapi.h>  // Mobile Broadband API; support begins with Windows 7.
#include <tchar.h>

#include "version.h"
#include "mobilebroadbanddevice.h"
#include "sim.h"
#include "util.h"

#define SIMINFO_ERROR_NO_DATA    L"(No data available)"
const std::string output_prefix = ">>> ";

bool    isSwitch (const char* str, const char* value);
bool    getParameterArgument (const char* str, const char* prefix, char* output);
void    printHelp ();

int main (int argc, char* argv[])
{
	/* You must initialize the COM library before calling any of its functions.
	Leave this job to the caller/calling application (this one)!
	See also <https://blogs.msdn.microsoft.com/larryosterman/2004/05/12/things-you-shouldnt-do-part-2-dlls-cant-ever-call-coinitialize-on-the-applications-thread/>. */

	HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);

	if (FAILED(hr))
	{
		_tprintf(_T("Error: COM initialization failed (%x).\n"), hr);
		exit(1);
	}

	try
	{
		siminfo::MobileBroadbandDevice mbdevice;
		siminfo::SIM sim_card;
	
		std::string status;
	
		if (argc < 2 || isSwitch(argv[1], "h") || isSwitch(argv[1], "help") || isSwitch(argv[1], "?"))
		{
			printHelp();
		}
		else
		{
			if (mbdevice.isDeviceBlocked())
			{
				// Print message that the device/SIM is blocked, but don't exit;
				// so that the user has the chance to invoke "/unblockSIM".
				mbdevice.isDeviceReady(status);
				std::cout << output_prefix << status << std::endl;
			}
			else if (FAILED(mbdevice.getData()) || FAILED(sim_card.getData()))
			{
				mbdevice.isDeviceReady(status);
				std::cout << output_prefix << status << std::endl;
				return 1;
			}

			// Get the command line arguments; skip the first one (it's the filename of the executable).
			for (int i = 1; i < argc; i++)
			{
				if (isSwitch(argv[i], "UnlockSIM"))
				{
					//std::cout << output_prefix << sim_card.unlockSIM(argv[2]) << std::endl;
					_tprintf(_T("%hs%ls\n"), output_prefix.c_str(), sim_card.unlockSIM(argv[2]));
					break;
				}

				if (isSwitch(argv[i], "UnblockSIM"))
				{
					std::cout << output_prefix << sim_card.unblockSIM(argv[2], argv[3]) << std::endl;
					break;
				}

				if (isSwitch(argv[i], "ChangePIN"))
				{
					std::cout << output_prefix << sim_card.changePIN(argv[2], argv[3]) << std::endl;
					break;
				}

				if (isSwitch(argv[i], "EnablePIN"))
				{
					sim_card.enablePIN(true, argv[2]);
					break;
				}

				if (isSwitch(argv[i], "DisablePIN"))
				{
					sim_card.enablePIN(false, argv[2]);
					break;
				}

				if (isSwitch(argv[i], "All"))
				{
					_tprintf(_T("\n------------ SIM -------------------------------------------------------\n"));
					if (SysStringLen(sim_card.getProviderName()) == 0)
						_tprintf(_T("Provider           : %s\n"), SIMINFO_ERROR_NO_DATA);
					else
						_tprintf(_T("Provider           : %s\n"), sim_card.getProviderName());
					_tprintf(_T("ICCID              : %s\n"), sim_card.getICCID());
					_tprintf(_T("IMEI               : %s\n"), sim_card.getIMEI());
					if (SysStringLen(sim_card.getSubscriberID()) == 0)
						_tprintf(_T("Subscriber ID      : %s\n"), SIMINFO_ERROR_NO_DATA);
					else
						_tprintf(_T("Subscriber ID      : %s\n"), sim_card.getSubscriberID());
					if (SysStringByteLen(sim_card.telephone_numbers[0]) == 0)
						_tprintf(_T("Telephone number(s): %s\n"), SIMINFO_ERROR_NO_DATA);
					else
					{
						for (int i = 0; i < sizeof(sim_card.telephone_numbers) / sizeof(sim_card.telephone_numbers[0]); i++)
						{
							if (sim_card.telephone_numbers[i] != 0)
								_tprintf(_T("Telephone number   : %s\n"), sim_card.telephone_numbers[i]);
						}
					}

					_tprintf(_T("\n------------ Connection Profile (XML) ----------------------------------\n"));
					if (SysStringLen(mbdevice.ProfileXmlData) == 0)
						_tprintf(_T("Connection Profile: %s\n"), SIMINFO_ERROR_NO_DATA);
					else
						_tprintf(_T("%s\n"), mbdevice.ProfileXmlData);

					_tprintf(_T("\n------------ Mobile Broadband Device -----------------------------------\n"));
					_tprintf(_T("Signal strength       : %i%%\n"), mbdevice.getSignalStrength());
					_tprintf(_T("Celluar class         : %s\n"), mbdevice.getCelluarClassName());
					_tprintf(_T("Supported data classes: %s\n"), mbdevice.getSupportedDataClasses());
					_tprintf(_T("Current data class    : %s\n"), mbdevice.getCurrentDataClassName());
					_tprintf(_T("Custom data class     : %s\n"), mbdevice.getCustomDataClass());
					_tprintf(_T("Manufacturer          : %s\n"), mbdevice.getManufacturer());
					_tprintf(_T("Model                 : %s\n"), mbdevice.getModel());
					_tprintf(_T("Firmware info         : %s\n"), mbdevice.getFirmwareInfo());
					break;
				}

				if (isSwitch(argv[i], "ICCID"))
				{
					_tprintf(_T("%s"), sim_card.getICCID());
					break;
				}

				if (isSwitch(argv[i], "IMEI"))
				{
					_tprintf(_T("%s"), sim_card.getIMEI());
					break;
				}

				if (isSwitch(argv[i], "IMSI"))
				{
					_tprintf(_T("%s"), sim_card.getSubscriberID());
					break;
				}

				if (isSwitch(argv[i], "Profile"))
				{
					_tprintf(_T("%s"), mbdevice.ProfileXmlData);
					break;
				}

				if (isSwitch(argv[i], "updateProfile"))
				{
					if (SUCCEEDED(mbdevice.getData()))
					{
						mbdevice.prepareProfileWithDefaultValues(sim_card);

						for (int j = i + 1; j < argc; j++) // Skip argument 1 (program name) and 2 (the option)
						{
							// Get the paramters and override the profile data if neccessary.
							char r01[256] = { '\0' }, r02[256] = { '\0' }, r03[256] = { '\0' }, r04[256] = { '\0' }, r05[256] = { '\0' },
								r06[256] = { '\0' }, r07[256] = { '\0' }, r08[256] = { '\0' }, r09[256] = { '\0' }, r10[256] = { '\0' };

							if (getParameterArgument(argv[j], "Name", r01))
								mbdevice.new_connection_profile.Name = siminfo::charToBSTR(r01);

							if (getParameterArgument(argv[j], "IsDefault", r02))
								mbdevice.new_connection_profile.IsDefault = siminfo::charToBSTR(r02);

							if (getParameterArgument(argv[j], "ProfileCreationType", r03))
								mbdevice.new_connection_profile.ProfileCreationType = siminfo::charToBSTR(r03);

							if (getParameterArgument(argv[j], "AutoConnectOnInternet", r04))
								mbdevice.new_connection_profile.AutoConnectOnInternet = siminfo::charToBSTR(r04);

							if (getParameterArgument(argv[j], "ConnectionMode", r05))
								mbdevice.new_connection_profile.ConnectionMode = siminfo::charToBSTR(r05);

							if (getParameterArgument(argv[j], "AccessString", r06))
								mbdevice.new_connection_profile.AccessString = siminfo::charToBSTR(r06);

							if (getParameterArgument(argv[j], "UserName", r07))
								mbdevice.new_connection_profile.UserName = siminfo::charToBSTR(r07);

							if (getParameterArgument(argv[j], "Password", r08))
								mbdevice.new_connection_profile.Password = siminfo::charToBSTR(r08);

							if (getParameterArgument(argv[j], "Compression", r09))
								mbdevice.new_connection_profile.Compression = siminfo::charToBSTR(r09);

							if (getParameterArgument(argv[j], "AuthProtocol", r10))
								mbdevice.new_connection_profile.AuthProtocol = siminfo::charToBSTR(r10);
						}

						if (!mbdevice.makeNewProfile())
							return 1;
					}
					break;
				}

				if (isSwitch(argv[i], "TelNum"))
				{
					for (int i = 0; i < sizeof(sim_card.telephone_numbers) / sizeof(sim_card.telephone_numbers); i++)
					{
						if (sim_card.telephone_numbers[i] != 0)
							_tprintf(_T("%s"), sim_card.telephone_numbers[i]);
						else
							_tprintf(_T("%s"), SIMINFO_ERROR_NO_DATA);
					}
					break;
				}

				if (isSwitch(argv[i], "Signal"))
				{
					_tprintf(_T("%i%%"), mbdevice.getSignalStrength());
					break;
				}

				if (isSwitch(argv[i], "DataClass"))
				{
					_tprintf(_T("%s\n"), mbdevice.getCurrentDataClassName());
					_tprintf(_T("%s"), mbdevice.getCustomDataClass());
					break;
				}

				if (isSwitch(argv[i], "Connect"))
				{
					if (mbdevice.isDeviceReady(status))
						_tprintf(_T("%hs%ls\n"), output_prefix.c_str(), mbdevice.connect());
					else
						std::cout << output_prefix << status << std::endl;
					break;
				}

				if (isSwitch(argv[i], "Disconnect"))
				{
					if (mbdevice.isDeviceReady(status))
						_tprintf(_T("%hs%ls\n"), output_prefix.c_str(), mbdevice.disconnect());
					else
						std::cout << output_prefix << status << std::endl;
					break;
				}

				else
				{
					_tprintf(_T("Error: Unknown command.\n\nHere's the help again...\n"));
					printHelp();
					return 0;
				}
			}
		}	
	}
	catch (const std::exception& e)
	{
		std::cout << output_prefix << e.what() << "\n";
		return 1;
	}
	
	CoUninitialize(); // Closes the COM library, etc.
	return 0;
}


/* -------------------------------------------------------------------------------------------------
Checks if the string <str> represents the switch <value>.
Considers the switch characters '/' and '-'.
Case insensitive: Text is converted to UPPERCASE before comparison; only reliable for non-unicode text.
------------------------------------------------------------------------------------------------- */

bool isSwitch (const char* str, const char* value)
{
	std::string str_value(str);
	for (std::string::iterator i = str_value.begin(); i != str_value.end(); ++i)
		*i = ::toupper(*i);
		
	std::string switch_value_v1 ("/" + std::string(value));
	for (std::string::iterator i = switch_value_v1.begin(); i != switch_value_v1.end(); ++i)
		*i = ::toupper(*i);

	std::string switch_value_v2 ("-" + std::string(value));
	for (std::string::iterator i = switch_value_v2.begin(); i != switch_value_v2.end(); ++i)
		*i = ::toupper(*i);
	
	if ((str_value.compare(switch_value_v1) == 0) || (str_value.compare(switch_value_v2) == 0))
		return true;
	else
		return false;
}


/* -------------------------------------------------------------------------------------------------
Gets the argument of a parameter (that belongs to a switch/option), e.g. for /switchA parameterB=argumentC
this will put "argumentC" into <argument_output>.

Parameters:
str   : The command line argument that will be analyzed, e.g. argv[i].
prefix: The plain prefix for the parameter, without '=' or ':'.
output: Will contain the actual argument value..

Returns true if 'prefix' is found in 'str'.

Example: program /switch prefix=value

Considers the two common assignment characters: '=' and ':'.
Case insensitive: Text is converted to UPPERCASE before comparison; only reliable for non-unicode text.
------------------------------------------------------------------------------------------------- */

bool getParameterArgument (const char* str, const char* prefix, char* output)
{
	std::string str_value(str);
	for (std::string::iterator i = str_value.begin(); i != str_value.end(); ++i)
		*i = ::toupper(*i);
		
	std::string parameter_prefix_v1 (std::string(prefix) + "=");
	for (std::string::iterator i = parameter_prefix_v1.begin(); i != parameter_prefix_v1.end(); ++i)
		*i = ::toupper(*i);

	std::string parameter_prefix_v2 (std::string(prefix) + ":");
	for (std::string::iterator i = parameter_prefix_v2.begin(); i != parameter_prefix_v2.end(); ++i)
		*i = ::toupper(*i);
	
	if ((str_value.compare(0, parameter_prefix_v1.size(), parameter_prefix_v1) == 0) || (str_value.compare(0, parameter_prefix_v2.size(), parameter_prefix_v2) == 0))
	{
		size_t value_length = strlen(str) - strlen(parameter_prefix_v1.c_str());
		strncpy(output, str + strlen(parameter_prefix_v1.c_str()), value_length);
		return true;
	}
	else
	{
		return false;
	}
}


/* -------------------------------------------------------------------------------------------------
Prints the help for/info about this program.
------------------------------------------------------------------------------------------------- */

void printHelp ()
{
	std::ifstream lic_file("License.txt");
	std::string lic_content;
	
	if (lic_file.is_open())
	{
		std::stringstream lic_buf;
		lic_buf << lic_file.rdbuf();
		lic_content = lic_buf.str();
	}
	else
	{
		lic_content = "Error: Could not open file 'License.txt'!";
	}
	
	_tprintf(_T("\n\
%s %s - %s\n\n\
                                         # # #\n\
\n\
Usage: %s [/option]\n\
\n\
  /All      : Display all information at once.\n\
  /ICCID    : Display the SIM card's Integrated Circuit Card Identifier.\n\
  /IMEI     : Display the SIM card's International Mobile Station Equipment Identity.\n\
  /IMSI     : Display the SIM card's International Mobile Subscriber Identity.\n\
  /TelNum   : Display the telephone number(s) associated with the SIM card.\n\
  /Signal   : Display the approximated signal strength received by the device.\n\
  /DataClass: Display the data class currently used by the device.\n\
  /Profile  : Display the connection profile data in XML format.\n\
\n\
  /Connect   : Connect to the WWAN, using the default profile.\n\
  /Disconnect: Disconnect from the WWAN.\n\
\n\
  /UnlockSIM <PIN>            : Unlock the SIM card with the provided PIN.\n\
  /UnblockSIM <PUK> <NewPIN>  : Unblock the SIM card with the provided PUK\n\
                                and sets the PIN to the value of NewPIN.\n\
  /ChangePIN <CurPIN> <NewPIN>: Change the PIN of the SIM card.\n\
  /EnablePIN <PIN>            : Enable the PIN authentication of the SIM.\n\
  /DisablePIN <PIN>           : Disable the PIN authentication of the SIM.\n\
\n\
  /UpdateProfile [Key=Value] [Key=Value] [...]\n\
\n\
     Changes the values of the profile or creates a new/initial one.\n\
	 \n\
     Not all values must be provided, but the default values\n\
     might not be working with your provider.\n\
	 \n\
     Program returns code \"1\" on failure.\n\
     \n\
     IMPORTANT: The exact upper/lower case of each value does matter (e.g. \"MsCHAPv2\").\n\
     Otherwise the new profile might not be created at all!\n\
     \n\
     Key                  | Valid values\n\
     ---------------------+-------------------------------\n\
     Name                 | <text>\n\
     IsDefault            | {true | false}\n\
     ProfileCreationType  | {UserProvisioned | AdminProvisioned |\n\
                          |  OperatorProvisioned | DeviceProvisioned}\n\
     AutoConnectOnInternet| {true | false}\n\
     ConnectionMode       | {manual | auto | auto-home}\n\
     AccessString         | <text>\n\
     UserName             | <text>\n\
     Password             | <text>\n\
     Compression          | {ENABLE | DISABLE}\n\
     AuthProtocol         | {NONE | PAP | CHAP | MsCHAPv2}\n\
     ---------------------+-------------------------------\n\
\n\
                                         # # #\n\n\
%hs\n\
                                         # # #\n\n\
Visit <%s> for more information.\n\
"), APP_NAME, VERSION_STRING, APP_DESCRIPTION, APP_FILENAME, lic_content.c_str(), APP_HOMEPAGE);
}
