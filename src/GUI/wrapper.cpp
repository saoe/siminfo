/* =================================================================================================
A (C++) wrapper to use code from an unmanged C++ DLL in a C# project.

Copyright © 2018-2019 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
================================================================================================= */

#include "wrapper.h"

using namespace System;
using namespace System::Runtime::InteropServices;

namespace siminfo
{

MobileBroadbandDevice* CreateMobileBroadbandDeviceObject ()
{
	try
	{
		return new MobileBroadbandDevice();
	}
	catch (std::exception e)
	{
		System::String^ managed_string = System::Runtime::InteropServices::Marshal::PtrToStringAnsi((System::IntPtr)(char*) e.what());
		throw gcnew System::Exception(managed_string);
	}
}


void DeleteMobileBroadbandDeviceObject (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
	{
		delete objptr;
		objptr = 0;
	}
}


int getMobileBroadbandDeviceData (MobileBroadbandDevice* objptr)
{
	try
	{
		int r;

		if (objptr != 0)
		{
			r = objptr->getData();
		}

		return r;
	}
	catch (std::exception e)
	{
		System::String^ managed_string = System::Runtime::InteropServices::Marshal::PtrToStringAnsi((System::IntPtr)(char*) e.what());
		throw gcnew System::Exception(managed_string);
	}
}


BSTR getCelluarClassName (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
		return objptr->getCelluarClassName();
	else
		return ::SysAllocString(L"");
}


BSTR getCurrentDataClassName (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
		return objptr->getCurrentDataClassName();
	else
		return ::SysAllocString(L"");
}


BSTR getCustomDataClass (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
		return objptr->getCustomDataClass();
	else
		return ::SysAllocString(L"");
}


BSTR getSupportedDataClasses (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
		return objptr->getSupportedDataClasses();
	else
		return ::SysAllocString(L"");
}


BSTR getManufacturer (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
		return objptr->getManufacturer();
	else
		return ::SysAllocString(L"");
}


BSTR getModel (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
		return objptr->getModel();
	else
		return ::SysAllocString(L"");
}


BSTR getFirmwareInfo (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
		return objptr->getFirmwareInfo();
	else
		return ::SysAllocString(L"");
}


int getSignalStrength (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
		return objptr->getSignalStrength();
	else
		return 0;
}


BSTR connect (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
		return objptr->connect();
	else
		return ::SysAllocString(L"");
}


BSTR disconnect (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
		return objptr->disconnect();
	else
		return ::SysAllocString(L"");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

BSTR getProfileData (MobileBroadbandDevice* objptr)
{
	if (objptr != 0)
		return objptr->ProfileXmlData;
	else
		return ::SysAllocString(L"");
}


void makeNewProfile (MobileBroadbandDevice* objptr, SIM* simptr, 
	BSTR New_Connection_Profile_Name,
	BSTR New_Connection_Profile_IsDefault,
	BSTR New_Connection_Profile_ProfileCreationType,
	BSTR New_Connection_Profile_AutoConnectOnInternet,
	BSTR New_Connection_Profile_ConnectionMode,
	BSTR New_Connection_Profile_AccessString,
	BSTR New_Connection_Profile_UserName,
	BSTR New_Connection_Profile_Password,
	BSTR New_Connection_Profile_Compression,
	BSTR New_Connection_Profile_AuthProtocol
)
{
	if (objptr != 0)
	{
		objptr->prepareProfileWithDefaultValues(*simptr);
		
		SysFreeString(objptr->new_connection_profile.Name);
		objptr->new_connection_profile.Name                  = SysAllocString(New_Connection_Profile_Name);
		
		SysFreeString(objptr->new_connection_profile.IsDefault);
		objptr->new_connection_profile.IsDefault             = SysAllocString(New_Connection_Profile_IsDefault);
		
		SysFreeString(objptr->new_connection_profile.ProfileCreationType);
		objptr->new_connection_profile.ProfileCreationType   = SysAllocString(New_Connection_Profile_ProfileCreationType);
		
		SysFreeString(objptr->new_connection_profile.AutoConnectOnInternet);
		objptr->new_connection_profile.AutoConnectOnInternet = SysAllocString(New_Connection_Profile_AutoConnectOnInternet);
		
		SysFreeString(objptr->new_connection_profile.ConnectionMode);
		objptr->new_connection_profile.ConnectionMode        = SysAllocString(New_Connection_Profile_ConnectionMode);
		
		SysFreeString(objptr->new_connection_profile.AccessString);
		objptr->new_connection_profile.AccessString          = SysAllocString(New_Connection_Profile_AccessString);
		
		SysFreeString(objptr->new_connection_profile.UserName);
		objptr->new_connection_profile.UserName              = SysAllocString(New_Connection_Profile_UserName);
		
		SysFreeString(objptr->new_connection_profile.Password);
		objptr->new_connection_profile.Password              = SysAllocString(New_Connection_Profile_Password);
		
		SysFreeString(objptr->new_connection_profile.Compression);
		objptr->new_connection_profile.Compression           = SysAllocString(New_Connection_Profile_Compression);
		
		SysFreeString(objptr->new_connection_profile.AuthProtocol);
		objptr->new_connection_profile.AuthProtocol          = SysAllocString(New_Connection_Profile_AuthProtocol);

		objptr->makeNewProfile();
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////

SIM* CreateSIMObject ()
{
	try
	{
		return new SIM();
	}
	catch (std::exception e)
	{
		System::String^ managed_string = System::Runtime::InteropServices::Marshal::PtrToStringAnsi((System::IntPtr)(char*) e.what());
		throw gcnew System::Exception(managed_string);
	}
}


void DeleteSIMObject (SIM* objptr)
{
	if (objptr != 0)
	{
		delete objptr;
		objptr= 0;
	}
}


int getSIMData (SIM* objptr)
{
	int r;
	
	if (objptr != 0)
	{
		r = objptr->getData();
	}
	
	return r;
}


BSTR getProviderName (SIM* objptr)
{
	if (objptr != 0)
		return objptr->getProviderName();
	else
		return ::SysAllocString(L"");
}


BSTR getIMEI (SIM* objptr)
{
	if (objptr != 0)
		return objptr->getIMEI();
	else
		return ::SysAllocString(L"");
}


BSTR getICCID (SIM* objptr)
{
	if (objptr != 0)
		return objptr->getICCID();
	else
		return ::SysAllocString(L"");
}


BSTR getSubscriberID (SIM* objptr)
{
	if (objptr != 0)
		return objptr->getSubscriberID();
	else
		return ::SysAllocString(L"");
}


BSTR getTelNums (SIM* objptr)
{
	if (objptr != 0)
		return objptr->getTelNums();
	else
		return ::SysAllocString(L"");
}


BSTR unlockSIM (SIM* objptr, char* PIN)
{
	try
	{
		if (objptr != 0)
			return objptr->unlockSIM(PIN);
		else
			return ::SysAllocString(L"Error: unlockSIM() failed.");
	}
	catch (std::exception e)
	{
		System::String^ managed_string = System::Runtime::InteropServices::Marshal::PtrToStringAnsi((System::IntPtr)(char*) e.what());
		throw gcnew System::Exception(managed_string);
	}
}


} // namespace