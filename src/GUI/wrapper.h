/*==================================================================================================
A (C++) wrapper to use code from an unmanged C++ DLL in a C# project.

Copyright © 2018-2019 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
==================================================================================================*/

#include "sim.h"
#include "mobilebroadbanddevice.h"

namespace siminfo
{

extern "C"
{
	// --- Mobile Broadband Device ---
	__declspec(dllexport) MobileBroadbandDevice* CreateMobileBroadbandDeviceObject ();
	__declspec(dllexport) void DeleteMobileBroadbandDeviceObject (MobileBroadbandDevice* objptr);
	__declspec(dllexport) int getMobileBroadbandDeviceData (MobileBroadbandDevice* objptr);
	__declspec(dllexport) BSTR getCelluarClassName (MobileBroadbandDevice* objptr);
	__declspec(dllexport) BSTR getCurrentDataClassName (MobileBroadbandDevice* objptr);
	__declspec(dllexport) BSTR getCustomDataClass (MobileBroadbandDevice* objptr);
	__declspec(dllexport) BSTR getSupportedDataClasses (MobileBroadbandDevice* objptr);
	__declspec(dllexport) BSTR getManufacturer (MobileBroadbandDevice* objptr);
	__declspec(dllexport) BSTR getModel (MobileBroadbandDevice* objptr);
	__declspec(dllexport) BSTR getFirmwareInfo (MobileBroadbandDevice* objptr);
	__declspec(dllexport) int getSignalStrength (MobileBroadbandDevice* objptr);
	__declspec(dllexport) BSTR connect (MobileBroadbandDevice* objptr);
	__declspec(dllexport) BSTR disconnect (MobileBroadbandDevice* objptr);

	// --- Mobile Broadband Device/Connection Profile ---
	__declspec(dllexport) BSTR getProfileData (MobileBroadbandDevice* objptr);
	__declspec(dllexport) void makeNewProfile (MobileBroadbandDevice* objptr, SIM* simptr,
		BSTR New_Connection_Profile_Name,
		BSTR New_Connection_Profile_IsDefault,
		BSTR New_Connection_Profile_ProfileCreationType,
		BSTR New_Connection_Profile_AutoConnectOnInternet,
		BSTR New_Connection_Profile_ConnectionMode,
		BSTR New_Connection_Profile_AccessString,
		BSTR New_Connection_Profile_UserName,
		BSTR New_Connection_Profile_Password,
		BSTR New_Connection_Profile_Compression,
		BSTR New_Connection_Profile_AuthProtocol
	);

	// --- SIM ---
	__declspec(dllexport) SIM* CreateSIMObject ();
	__declspec(dllexport) void DeleteSIMObject (SIM* objptr);
	__declspec(dllexport) int getSIMData (SIM* objptr);
	__declspec(dllexport) BSTR getProviderName (SIM* objptr);
	__declspec(dllexport) BSTR getIMEI (SIM* objptr);
	__declspec(dllexport) BSTR getICCID (SIM* objptr);
	__declspec(dllexport) BSTR getSubscriberID (SIM* objptr);
	__declspec(dllexport) BSTR getTelNums (SIM* objptr);
	__declspec(dllexport) BSTR unlockSIM (SIM* objptr, char* PIN);
}

}
