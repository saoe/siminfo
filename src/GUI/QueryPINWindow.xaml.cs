/*==================================================================================================
Copyright © 2018 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
==================================================================================================*/

using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace siminfo
{

public partial class QueryPINWindow : Window
{
	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	public static extern string unlockSIM (IntPtr objptr, [MarshalAs(UnmanagedType.LPStr)] String PIN);

	public IntPtr ProxySIMObject;

	public QueryPINWindow (IntPtr ProxySIMObjectParameter)
	{
		InitializeComponent();

		ProxySIMObject = ProxySIMObjectParameter;
		OKButton.IsEnabled = false;
	}


	private void Input_TextChanged (object sender, TextChangedEventArgs e)
	{
		var tb = sender as TextBox;
		
		if (string.IsNullOrWhiteSpace(tb.Text))
		{ 
			OKButton.IsEnabled = false;
		}
		else
		{
			OKButton.IsEnabled = true;
		}
	}


	private void onClickedOK (object sender, RoutedEventArgs e)
	{
		try
		{
			//IntPtr x = unlockSIM(ProxySIMObject, Input.Text);
			//string managed_string = System.Runtime.InteropServices.Marshal.PtrToStringBSTR(x);
			//StatusBar.Text = managed_string;

			StatusBar.Text = unlockSIM(ProxySIMObject, Input.Text);
		}
		catch (Exception ex)
		{
			StatusBar.Text = ex.Message;
		}
	}
}

} // Namespace.
