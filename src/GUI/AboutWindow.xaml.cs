/*==================================================================================================
Copyright © 2018 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
==================================================================================================*/

using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using Microsoft.Win32;

namespace siminfo
{

public partial class AboutWindow : Window
{
	public AboutWindow ()
	{
		InitializeComponent();

		// Making the title bar text nice.
		this.Title = this.Title + " " + System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
		
		// Loading the license text from the file.
		string license_file_content;

		try
		{
			using (System.IO.StreamReader streamReader = new System.IO.StreamReader("License.txt", Encoding.UTF8))
			{
				license_file_content = streamReader.ReadToEnd();
			}
		}
		catch (Exception ex)
		{
			license_file_content = "Error: The license text could not be read:\n" + ex.Message;
		}

		// Composing the text that will be shown in the window.
		Hyperlink hyperlinked_name = new Hyperlink()
		{
			NavigateUri = new Uri(Constants.APP_HOMEPAGE)
		};
		hyperlinked_name.Inlines.Add(new Bold(new Run(Constants.APP_NAME + " " + Constants.VERSION_STRING)));
		hyperlinked_name.RequestNavigate += Hyperlink_RequestNavigate;

		this.AboutTextBox.Inlines.Add(hyperlinked_name);
		this.AboutTextBox.Inlines.Add("\r\n\r\n");
		this.AboutTextBox.Inlines.Add(Constants.APP_DESCRIPTION);
		this.AboutTextBox.Inlines.Add("\r\n\r\n");
		this.AboutTextBox.Inlines.Add(license_file_content);

		}

		
		// Event raised from RichTextBox when user clicks on a link.
		private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
		{
			System.Diagnostics.Process.Start(e.Uri.AbsoluteUri); // Opens the default browser.
			e.Handled = true;
		}
	}

} // Namespace.
