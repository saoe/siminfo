/*==================================================================================================
Copyright © 2018-2019 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
==================================================================================================*/

using System;
//using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace siminfo
{

public partial class UpdateProfileWindow : Window
{
	public IntPtr ProxyMobileBroadbandDeviceObject;
	public IntPtr ProxySIMObject;

	private readonly string[] IsDefaultValues = { "<undefined>", "true", "false" };
	private readonly string[] ProfileCreationTypeValues = { "<undefined>", "UserProvisioned", "AdminProvisioned", "OperatorProvisioned", "DeviceProvisioned" };
	private readonly string[] AutoConnectOnInternetValues = { "<undefined>", "true", "false" };
	private readonly string[] ConnectionModeValues = { "<undefined>", "manual", "auto", "auto-home" };
	private readonly string[] CompressionValues = { "<undefined>", "ENABLE", "DISABLE" };
	private readonly string[] AuthProtocolValues = { "<undefined>", "NONE", "PAP", "CHAP", "MsCHAPv2" };

	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_Name;
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_IsDefault;
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_ProfileCreationType;
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_SubscriberID;
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_SimIccID;
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_HomeProviderName; // From MSDN: "This element is optional and is set by the Mobile Broadband service for internal use. An application should not set this field while creating or updating a profile."
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_AutoConnectOnInternet;
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_ConnectionMode;
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_AccessString;
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_UserName;
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_Password;
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_Compression;
	[MarshalAs(UnmanagedType.BStr)] public String New_Connection_Profile_AuthProtocol;

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	public static extern void makeNewProfile (IntPtr objptr, IntPtr simptr,
		[MarshalAs(UnmanagedType.BStr)] String New_Connection_Profile_Name,
		[MarshalAs(UnmanagedType.BStr)] String New_Connection_Profile_IsDefault,
		[MarshalAs(UnmanagedType.BStr)] String New_Connection_Profile_ProfileCreationType,
		[MarshalAs(UnmanagedType.BStr)] String New_Connection_Profile_AutoConnectOnInternet,
		[MarshalAs(UnmanagedType.BStr)] String New_Connection_Profile_ConnectionMode,
		[MarshalAs(UnmanagedType.BStr)] String New_Connection_Profile_AccessString,
		[MarshalAs(UnmanagedType.BStr)] String New_Connection_Profile_UserName,
		[MarshalAs(UnmanagedType.BStr)] String New_Connection_Profile_Password,
		[MarshalAs(UnmanagedType.BStr)] String New_Connection_Profile_Compression,
		[MarshalAs(UnmanagedType.BStr)] String New_Connection_Profile_AuthProtocol
	);

	public UpdateProfileWindow (IntPtr ProxyMobileBroadbandDeviceObjectParameter, IntPtr ProxySIMObjectParameter)
	{
		InitializeComponent();
		ProxyMobileBroadbandDeviceObject = ProxyMobileBroadbandDeviceObjectParameter;
		ProxySIMObject = ProxySIMObjectParameter;

		Profile_IsDefault_ComboBox.ItemsSource = IsDefaultValues;
		Profile_IsDefault_ComboBox.SelectedIndex = 0;

		Profile_ProfileCreationType_ComboBox.ItemsSource = ProfileCreationTypeValues;
		Profile_ProfileCreationType_ComboBox.SelectedIndex = 0;
		
		Profile_AutoConnectOnInternet_ComboBox.ItemsSource = AutoConnectOnInternetValues;
		Profile_AutoConnectOnInternet_ComboBox.SelectedIndex = 0;
		
		Profile_ConnectionMode_ComboBox.ItemsSource = ConnectionModeValues;
		Profile_ConnectionMode_ComboBox.SelectedIndex = 0;
		
		Profile_Compression_ComboBox.ItemsSource = CompressionValues;
		Profile_Compression_ComboBox.SelectedIndex = 0;
		
		Profile_AuthProtocol_ComboBox.ItemsSource = AuthProtocolValues;
		Profile_AuthProtocol_ComboBox.SelectedIndex = 0;
	}


	private void Profile_Name_TextChanged (object sender, TextChangedEventArgs e)
	{
		New_Connection_Profile_Name = (sender as TextBox).Text;
	}

	private void Profile_IsDefault_SelectionChanged (object sender, SelectionChangedEventArgs e)
	{
		New_Connection_Profile_IsDefault = (sender as ComboBox).SelectedValue as string;
	}

	private void Profile_ProfileCreationType_SelectionChanged (object sender, SelectionChangedEventArgs e)
	{
		New_Connection_Profile_ProfileCreationType = (sender as ComboBox).SelectedValue as string;
	}

	private void Profile_AutoConnectOnInternet_SelectionChanged (object sender, SelectionChangedEventArgs e)
	{
		New_Connection_Profile_AutoConnectOnInternet = (sender as ComboBox).SelectedValue as string;
	}

	private void Profile_ConnectionMode_SelectionChanged (object sender, SelectionChangedEventArgs e)
	{
		New_Connection_Profile_ConnectionMode = (sender as ComboBox).SelectedValue as string;
	}

	private void Profile_AccessString_TextChanged (object sender, TextChangedEventArgs e)
	{
		New_Connection_Profile_AccessString = (sender as TextBox).Text;
	}

	private void Profile_UserName_TextChanged (object sender, TextChangedEventArgs e)
	{
		New_Connection_Profile_UserName = (sender as TextBox).Text;
	}

	private void Profile_Password_TextChanged (object sender, TextChangedEventArgs e)
	{
		New_Connection_Profile_Password = (sender as TextBox).Text;
	}

	private void Profile_Compression_SelectionChanged (object sender, SelectionChangedEventArgs e)
	{
		New_Connection_Profile_Compression = (sender as ComboBox).SelectedValue as string;
	}

	private void Profile_AuthProtocol_SelectionChanged (object sender, SelectionChangedEventArgs e)
	{
		New_Connection_Profile_AuthProtocol = (sender as ComboBox).SelectedValue as string;
	}

	private void onClickedOK (object sender, RoutedEventArgs e)
	{
		makeNewProfile (ProxyMobileBroadbandDeviceObject, ProxySIMObject,
			New_Connection_Profile_Name,
			New_Connection_Profile_IsDefault,
			New_Connection_Profile_ProfileCreationType,
			New_Connection_Profile_AutoConnectOnInternet,
			New_Connection_Profile_ConnectionMode,
			New_Connection_Profile_AccessString,
			New_Connection_Profile_UserName,
			New_Connection_Profile_Password,
			New_Connection_Profile_Compression,
			New_Connection_Profile_AuthProtocol);

		this.Close();
	}
}

} // Namespace.
