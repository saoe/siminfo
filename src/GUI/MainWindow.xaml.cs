/*==================================================================================================
Copyright © 2018-2019 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
==================================================================================================*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Globalization;

namespace siminfo
{

public partial class MainWindow : Window
{
	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	private static extern IntPtr CreateMobileBroadbandDeviceObject ();

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	private static extern void DeleteMobileBroadbandDeviceObject (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	private static extern int getMobileBroadbandDeviceData (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getCelluarClassName (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getCurrentDataClassName (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getCustomDataClass (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getSupportedDataClasses (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getManufacturer (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getModel (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getFirmwareInfo (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	private static extern int getSignalStrength (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string connect (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string disconnect (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	private static extern IntPtr CreateSIMObject ();

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	private static extern void DeleteSIMObject (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	private static extern int getSIMData (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getProviderName (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getIMEI (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getICCID (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getSubscriberID (IntPtr objptr);

	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	private static extern string getTelNums (IntPtr objptr);

	public IntPtr ProxyMobileBroadbandDeviceObject;
	public IntPtr ProxySIMObject;

	public MainWindow ()
	{
		InitializeComponent();
		
		this.Title = System.Reflection.Assembly.GetCallingAssembly().GetName().Name + " "
		           + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + "."
		           + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor;
		try
		{
			// Preparing the proxy/wrapper object for P/Invoke method calls.
			ProxyMobileBroadbandDeviceObject = CreateMobileBroadbandDeviceObject();
			ProxySIMObject = CreateSIMObject();
		
			getMobileBroadbandDeviceData(ProxyMobileBroadbandDeviceObject);
			getSIMData(ProxySIMObject);

			SIM_Provider.Text = getProviderName(ProxySIMObject);
			SIM_ICCID.Text = getICCID(ProxySIMObject);
			SIM_IMEI.Text = getIMEI(ProxySIMObject);
			SIM_Subscriber_ID.Text = getSubscriberID(ProxySIMObject);
			SIM_Telephone_Numbers.Text = getTelNums(ProxySIMObject);

			MBN_Celluar_Class.Text = getCelluarClassName(ProxyMobileBroadbandDeviceObject);
			MBN_Supported_Data_Classes.Text = getSupportedDataClasses(ProxyMobileBroadbandDeviceObject);
			MBN_Data_Class.Text = getCurrentDataClassName(ProxyMobileBroadbandDeviceObject);
			MBN_Custom_Data_Class.Text = getCustomDataClass(ProxyMobileBroadbandDeviceObject);
			MBN_Manufacturer.Text = getManufacturer(ProxyMobileBroadbandDeviceObject);
			MBN_Model.Text = getModel(ProxyMobileBroadbandDeviceObject);
			MBN_FirmwareInfo.Text = getFirmwareInfo(ProxyMobileBroadbandDeviceObject);
		
			// Note: Set the progressbar range value and the overlaying text in one sweep.
			MBN_Signal_Strength_Textblock.Text = Convert.ToString(MBN_Signal_Strength_Progressbar.Value = getSignalStrength(ProxyMobileBroadbandDeviceObject)) + "%";
		}
		catch (Exception e)
		{
			SIMGroupControls.IsEnabled = false;
			ConnectionProfileGroupControls.IsEnabled = false;
			MobileBroadbandDeviceGroupControls.IsEnabled = false;

			StatusBar.Text = e.Message;
		}
	}


	~MainWindow ()
	{
		// Cleaning up the proxy object again.
		DeleteSIMObject(ProxySIMObject);
		ProxySIMObject = IntPtr.Zero;
		
		DeleteMobileBroadbandDeviceObject(ProxyMobileBroadbandDeviceObject);
		ProxyMobileBroadbandDeviceObject = IntPtr.Zero;
	}


	// Text displayed in the 'About' box.
	private void showAboutWindow (object sender, RoutedEventArgs e)
	{
		// 'Using {Owner = this}', so that the WindowStartupLocation="CenterOwner" in the XAML file will work correctly.
		AboutWindow aw = new AboutWindow() { Owner = this };
		aw.ShowDialog();
	}

	private void showChangePINWindow ()
	{
		// 'Using {Owner = this}', so that the WindowStartupLocation="CenterOwner" in the XAML file will work correctly.
		ChangePINWindow cpw = new ChangePINWindow() { Owner = this };
		cpw.ShowDialog();
	}
	
	private void showQueryPINWindow ()
	{
		// 'Using {Owner = this}', so that the WindowStartupLocation="CenterOwner" in the XAML file will work correctly.
		QueryPINWindow qpw = new QueryPINWindow(ProxySIMObject) { Owner = this };
		qpw.ShowDialog();
	}

	private void showQueryPUKWindow ()
	{
		// 'Using {Owner = this}', so that the WindowStartupLocation="CenterOwner" in the XAML file will work correctly.
		QueryPUKWindow qpw = new QueryPUKWindow() { Owner = this };
		qpw.ShowDialog();
	}

	private void showProfileWindow()
	{
		// 'Using {Owner = this}', so that the WindowStartupLocation="CenterOwner" in the XAML file will work correctly.
		ProfileWindow w = new ProfileWindow(ProxyMobileBroadbandDeviceObject) { Owner = this };
		w.ShowDialog();
	}

	private void showUpdateProfileWindow()
	{
		// 'Using {Owner = this}', so that the WindowStartupLocation="CenterOwner" in the XAML file will work correctly.
		UpdateProfileWindow w = new UpdateProfileWindow(ProxyMobileBroadbandDeviceObject, ProxySIMObject) { Owner = this };
		w.ShowDialog();
	}

	private void ChangePIN_button_clicked (object sender, RoutedEventArgs e)
	{
		showChangePINWindow();
	}

	private void EnablePIN_button_clicked (object sender, RoutedEventArgs e)
	{
		StatusBar.Text = "EnablePIN_button_clicked";
	}

	private void DisablePIN_button_clicked (object sender, RoutedEventArgs e)
	{
		StatusBar.Text = "DisablePIN_button_clicked";
	}
	
	private void UnlockSIM_button_clicked (object sender, RoutedEventArgs e)
	{
		showQueryPINWindow();
	}

	private void UnblockSIM_button_clicked (object sender, RoutedEventArgs e)
	{
		showQueryPUKWindow();
	}

	private void ShowProfile_button_clicked (object sender, RoutedEventArgs e)
	{
		showProfileWindow();
	}

	private void UpdateProfile_button_clicked(object sender, RoutedEventArgs e)
	{
		showUpdateProfileWindow();
	}

	private void Connect_button_clicked (object sender, RoutedEventArgs e)
	{
		StatusBar.Text = "";
		StatusBar.Text = connect(ProxyMobileBroadbandDeviceObject);
	}

	private void Disconnect_button_clicked (object sender, RoutedEventArgs e)
	{
		StatusBar.Text = "";
		StatusBar.Text = disconnect(ProxyMobileBroadbandDeviceObject);
	}
}

} // Namespace.
