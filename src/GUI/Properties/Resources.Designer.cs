﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to About.
        /// </summary>
        public static string AboutWindowTitle {
            get {
                return ResourceManager.GetString("AboutWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change PIN.
        /// </summary>
        public static string ChangePIN {
            get {
                return ResourceManager.GetString("ChangePIN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change PIN.
        /// </summary>
        public static string ChangePINWindowTitle {
            get {
                return ResourceManager.GetString("ChangePINWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm new PIN:.
        /// </summary>
        public static string ConfirmNewPIN {
            get {
                return ResourceManager.GetString("ConfirmNewPIN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connect.
        /// </summary>
        public static string Connect {
            get {
                return ResourceManager.GetString("Connect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deactivate PIN.
        /// </summary>
        public static string DisablePIN {
            get {
                return ResourceManager.GetString("DisablePIN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Disconnect.
        /// </summary>
        public static string Disconnect {
            get {
                return ResourceManager.GetString("Disconnect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activate PIN.
        /// </summary>
        public static string EnablePIN {
            get {
                return ResourceManager.GetString("EnablePIN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter new PIN:.
        /// </summary>
        public static string EnterNewPIN {
            get {
                return ResourceManager.GetString("EnterNewPIN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter current PIN:.
        /// </summary>
        public static string EnterPIN {
            get {
                return ResourceManager.GetString("EnterPIN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter PUK:.
        /// </summary>
        public static string EnterPUK {
            get {
                return ResourceManager.GetString("EnterPUK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Celluar Class:.
        /// </summary>
        public static string MBN_Celluar_Class {
            get {
                return ResourceManager.GetString("MBN_Celluar_Class", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Custom Data Class.
        /// </summary>
        public static string MBN_Custom_Data_Class {
            get {
                return ResourceManager.GetString("MBN_Custom_Data_Class", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Current Data Class:.
        /// </summary>
        public static string MBN_Data_Class {
            get {
                return ResourceManager.GetString("MBN_Data_Class", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Firmware Info:.
        /// </summary>
        public static string MBN_FirmwareInfo {
            get {
                return ResourceManager.GetString("MBN_FirmwareInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manufacturer:.
        /// </summary>
        public static string MBN_Manufacturer {
            get {
                return ResourceManager.GetString("MBN_Manufacturer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Model:.
        /// </summary>
        public static string MBN_Model {
            get {
                return ResourceManager.GetString("MBN_Model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Signal Strength:.
        /// </summary>
        public static string MBN_Signal_Strength {
            get {
                return ResourceManager.GetString("MBN_Signal_Strength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This value is most certainly too low, if you&apos;re using a network connection better than GSM (2G):
        ///
        ///Microsoft&apos;s Mbn API returns a value based on a wrong scale for UMTS (3G) and better networks.
        ///
        ///In that case, the signal strength is better than displayed here....
        /// </summary>
        public static string MBN_Signal_Strength_Tooltip {
            get {
                return ResourceManager.GetString("MBN_Signal_Strength_Tooltip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Supported Data Classes:.
        /// </summary>
        public static string MBN_Supported_Data_Classes {
            get {
                return ResourceManager.GetString("MBN_Supported_Data_Classes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        public static string OK {
            get {
                return ResourceManager.GetString("OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PIN Query:.
        /// </summary>
        public static string PIN_Query {
            get {
                return ResourceManager.GetString("PIN_Query", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Active.
        /// </summary>
        public static string PIN_Query_Active {
            get {
                return ResourceManager.GetString("PIN_Query_Active", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Inactive.
        /// </summary>
        public static string PIN_Query_Inactive {
            get {
                return ResourceManager.GetString("PIN_Query_Inactive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unlocking SIM.
        /// </summary>
        public static string QueryPINWindowTitle {
            get {
                return ResourceManager.GetString("QueryPINWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unblocking SIM .
        /// </summary>
        public static string QueryPUKWindowTitle {
            get {
                return ResourceManager.GetString("QueryPUKWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show Profile.
        /// </summary>
        public static string ShowProfile {
            get {
                return ResourceManager.GetString("ShowProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Current Connection Profile.
        /// </summary>
        public static string ShowProfileWindowTitle {
            get {
                return ResourceManager.GetString("ShowProfileWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ICCID:.
        /// </summary>
        public static string SIM_ICCID {
            get {
                return ResourceManager.GetString("SIM_ICCID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IMEI:.
        /// </summary>
        public static string SIM_IMEI {
            get {
                return ResourceManager.GetString("SIM_IMEI", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Provider:.
        /// </summary>
        public static string SIM_Provider {
            get {
                return ResourceManager.GetString("SIM_Provider", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subscriber ID:.
        /// </summary>
        public static string SIM_Subscriber_ID {
            get {
                return ResourceManager.GetString("SIM_Subscriber_ID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Telephone Number(s):.
        /// </summary>
        public static string SIM_Telephone_Numbers {
            get {
                return ResourceManager.GetString("SIM_Telephone_Numbers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unblock with PUK.
        /// </summary>
        public static string UnblockSIM {
            get {
                return ResourceManager.GetString("UnblockSIM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unlock with PIN.
        /// </summary>
        public static string UnlockSIM {
            get {
                return ResourceManager.GetString("UnlockSIM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Profile....
        /// </summary>
        public static string UpdateProfile {
            get {
                return ResourceManager.GetString("UpdateProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Profile.
        /// </summary>
        public static string UpdateProfileWindowTitle {
            get {
                return ResourceManager.GetString("UpdateProfileWindowTitle", resourceCulture);
            }
        }
    }
}
