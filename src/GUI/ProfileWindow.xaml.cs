/*==================================================================================================
Copyright © 2018 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
==================================================================================================*/

using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace siminfo
{

public partial class ProfileWindow : Window
{
	[DllImport("CLR_Wrapper.dll", CallingConvention = CallingConvention.Cdecl)]
	[return: MarshalAs(UnmanagedType.BStr)]
	public static extern string getProfileData (IntPtr objptr);

	public IntPtr ProxyMobileBroadbandDeviceObject;

	public ProfileWindow (IntPtr ProxyMobileBroadbandDeviceObjectParameter)
	{
		InitializeComponent();
		ProxyMobileBroadbandDeviceObject = ProxyMobileBroadbandDeviceObjectParameter;
		XmlDataTextBlock.Text = getProfileData(ProxyMobileBroadbandDeviceObject);
	}
}

} // Namespace.
