using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Windows;

namespace siminfo
{
    public partial class App : Application
    {
		// Custom main method (replacing the StartupUri="MainWindow.xaml" from App.xaml; got removed).

		/* [STAThread] changes the apartment state of the current thread to be single threaded; needed for COM support in Windows Forms.
		Also note: "Does the CLR really call CoInitializeEx on the first call to unmanaged code, even if you don�t deal with COM at all
		and are just calling native code via p/invoke?" --> YES. <https://blogs.msdn.microsoft.com/oldnewthing/20150316-00/?p=44463>
		
		Update after switch from WinForms to WPF:
		"STA thread is the default attribute of Main method when we create new WPF application using either Visual Studio or Visual C# Express." */
		[STAThread]
		public static void Main()
		{
			App app = new App();
			app.InitializeComponent();

			MainWindow window = new MainWindow();
			
			app.Run(window);
		}
	}
}
