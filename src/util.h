/* =================================================================================================
Copyright © 2015-2017 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
================================================================================================= */

#ifndef SIMINFO_UTIL_H
#define SIMINFO_UTIL_H

#include <string>
#include <tchar.h>

namespace siminfo
{

BSTR concatenateBSTR (BSTR a, BSTR b);
BSTR charToBSTR (char* input);
std::string convertBSTRToMBS (BSTR bstr);

} // Namespace.

#endif