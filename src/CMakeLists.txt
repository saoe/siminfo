# ----------------------------------------------------------------------------------
# Copyright © 2015-2019 Sascha Offe <so@saoe.net>.
# Published under the terms of the MIT license <http://opensource.org/licenses/MIT>.
#
# Remark: Only Microsoft Visual Studio 2017 is currently supported.
#
# Usage:
#
# 1. Go to the top-level directory of your project and create a sub-directory for out-of-source build.
#    C:\ProjectX> mkdir build
#
# 2. Go to that sub-directory and run cmake, indicating where to finde CMakeLists.txt.
#    and which Generator to use (e.g. Visual Studio 2017, 64-bit build)
#    C:\ProjectX> cd build
#    C:\ProjectX\build> cmake ..\src -G "Visual Studio 15 2017 Win64"
#
# 3. a) Use the *.sln files to open the project with Visual Studio and build the targets.
#                             ~ or ~
#    b) Use CMake to trigger a build on the command line; for example:
#       C:\ProjectX\build> cmake --build . --target ALL_BUILD --config Release --parallel
#           (Option --parallel/-j is new in 3.12: The maximum number of concurrent processes to use when building.)
#           (Note that --build is just a front end for invoking the native build tools.)
# ----------------------------------------------------------------------------------

cmake_minimum_required (VERSION 3.8.2) # Official support for C# project generation starts here.

# Limit the MSVC configuration types to "Release" and "Debug". Must be this far up in the file!
set (CMAKE_CONFIGURATION_TYPES Release Debug CACHE TYPE INTERNAL FORCE)

string (TIMESTAMP CURRENT_YEAR "%Y")

# Get the path of the directory one level above this CMakeLists.txt file (which is already the top-most that CMake knows about).
get_filename_component (ONE_DIRECTORY_ABOVE ../ ABSOLUTE)

# -------- Global project variables. ------------------------
project ("SIMInfo")

set (SIMINFO_DEVEL_DIR "C:/devel")
set (SIMINFO_OUTPUT_DIR ${PROJECT_BINARY_DIR}/_Output)

set (VERSION_MAJOR     2 CACHE STRING "Major version number.")
set (VERSION_MINOR     1 CACHE STRING "Minor version number.")
set (VERSION_STRING    "${VERSION_MAJOR}.${VERSION_MINOR}")
set (APP_NAME          ${PROJECT_NAME})
set (APP_FILENAME      ${PROJECT_NAME}${CMAKE_EXECUTABLE_SUFFIX})
set (APP_DESCRIPTION   "Utility for SIM card and mobile broadband connection profile.")
set (APP_LICENSE       "Published under the terms of the MIT license <https://opensource.org/licenses/MIT>.")
set (APP_HOMEPAGE      "http://www.saoe.net/software/")
set (AUTHOR_NAME       "Sascha Offe")
set (AUTHOR_MAIL       "so@saoe.net")
set (COPYRIGHT         "Copyright (c) 2015-${CURRENT_YEAR} ${AUTHOR_NAME}.")
set (LICENSE_FILE      License.txt)

# ----------------------------------------------------------------

# Popular CMake idiom to check whether the build is done for(!) a 64-bit target.
# Uses only the common x86 and x64 symbols at the moment, no IA-64, AMD64, X86_64 etc.
if (${CMAKE_SIZEOF_VOID_P} EQUAL "8")
	set (SIMINFO_Architecture "x64")
else ()
	set (SIMINFO_Architecture "x86")
endif ()

# Turn on the ability to create folders to organize projects (.vcproj)
# Creates "CMakePredefinedTargets" folder by default and adds CMake defined projects like INSTALL.vcproj and ZERO_CHECK.vcproj
set_property (GLOBAL PROPERTY USE_FOLDERS ON)

if (MSVC_VERSION GREATER_EQUAL 1910)                                # - Microsoft Visual Studio 2017 (15.x)
	if (CMAKE_CL_64 OR ${SIMINFO_Architecture} EQUAL "x64")         # -- Configuration for 64-bit builds.
	else ()                                                         # -- Configuration for 32-bit builds.
	endif ()
	
	add_subdirectory (Library)
	add_subdirectory (CLI)
	add_subdirectory (GUI)
else ()	
	message(FATAL_ERROR "\nError: Only Microsoft Visual Studio is currently supported.\n")
endif ()
