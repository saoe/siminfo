/* =================================================================================================
Copyright © 2015-2017 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
================================================================================================= */

#include <Windows.h>
#include <tchar.h>

#include "util.h"

namespace siminfo
{

/* -------------------------------------------------------------------------------------------------
Helper function to concatenate two BSTRs ("basic strings"). 

Regarding BSTRs: "Before [it] goes out of scope, its memory needs to be released by calling ::SysFreeString()."

Also worth reading: http://www.johndcook.com/blog/cplusplus_strings/
                    https://msdn.microsoft.com/de-de/library/xda6xzx7.aspx
------------------------------------------------------------------------------------------------- */

BSTR concatenateBSTR (BSTR a, BSTR b)
{
	unsigned int length_a = ::SysStringLen(a);
    unsigned int length_b = ::SysStringLen(b);

    BSTR result = ::SysAllocStringLen(NULL, length_a + length_b);

    memcpy(result, a, length_a * sizeof(OLECHAR));
    memcpy(result + length_a, b, length_b * sizeof(OLECHAR));

    result[length_a + length_b] = 0;
    return result;
}


/* -------------------------------------------------------------------------------------------------
Convert char* string to BSTR string.
Note that this leaks memory because we don't (can't) call ::SysFreeString()!
------------------------------------------------------------------------------------------------- */

BSTR charToBSTR (char* input)
{
	BSTR output;
	int wslen = MultiByteToWideChar(CP_ACP, 0, input, strlen(input), 0, 0);
	output = SysAllocStringLen(0, wslen);
	MultiByteToWideChar(CP_ACP, 0, input, strlen(input), output, wslen);
	return output;
}


/* -------------------------------------------------------------------------------------------------
Convert a BSTR string to a MultiByteString/std::string.
------------------------------------------------------------------------------------------------- */

std::string convertWCSToMBS (const wchar_t* pstr, long wslen)
{
	int len = ::WideCharToMultiByte(CP_ACP, 0, pstr, wslen, NULL, 0, NULL, NULL);

	std::string dblstr(len, '\0');
	len = ::WideCharToMultiByte(CP_ACP, 0 /* no flags */,
	                            pstr, wslen /* not necessary NULL-terminated */,
	                            &dblstr[0], len,
	                            NULL, NULL /* no default char */);

    return dblstr;
}

std::string convertBSTRToMBS (BSTR bstr)
{
	int wslen = ::SysStringLen(bstr);
	return convertWCSToMBS((wchar_t*)bstr, wslen);
}

} // Namespace.
