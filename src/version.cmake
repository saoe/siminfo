# Copyright 2015 Sascha Offe <so@saoe.net>.
# Published under the terms of the MIT license <http://opensource.org/licenses/MIT>.

# Timestamp of the build (local time; append the optional parameter 'UTC' for Coordinated Universal Time).
string (TIMESTAMP BUILDTIME "%Y-%m-%d %H:%M:%S")

# Mercurial (Hg) Changeset ID: Full length (unique) identifier of the current working copy.
execute_process (
     COMMAND cmd /c hg --debug id -i
     OUTPUT_VARIABLE HG_CHANGESET_ID
     OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Substitute the placeholders with real values and create a usable header file.
configure_file (${IN} ${OUT} @ONLY)
