' Simple GUI for unlocking the SIM and setting the Telekom.de APN.

input = InputBox("Bitte PIN zum Entsperren der SIM eingeben:", "Telekom APN setzen")
 

Set oWshShell = CreateObject("WScript.Shell")
ret_pin = oWshShell.Run("siminfo.exe /unlock " & input, 0, True)
 

if ret_pin = 1 then
	MsgBox "Falsche PIN (" + input + ") oder WWAN-Modem wurde nicht erkannt!", 0, "Fehler"
	Wscript.Quit
else
	WScript.Sleep 3000
	ret_apn = oWshShell.Run("siminfo.exe /updateProfile Name=Telekom.de IsDefault=true ProfileCreationType=UserProvisioned AutoConnectOnInternet=false ConnectionMode=manual AccessString=internet.t-mobile UserName=tmobile Password=tm Compression=DISABLE AuthProtocol=PAP", 0, True)

	if ret_apn = 1 then
		MsgBox "Der APN konnte nicht gesetzt werden!", 0, "Fehler"
	else
		MsgBox "OK, APN gesetzt!", 0, "OK"
	end if
end if
