/*==================================================================================================
Copyright � 2015-2018 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
==================================================================================================*/

#include <stdexcept>
#include <atlbase.h>
#include "sim.h"
#include "util.h"

namespace siminfo
{

SIM::SIM ()
{
	HRESULT hr;	

	/* Note: You must initialize the COM library on a thread before you call any of the library functions (like CoCreateInstance(),
	but do not do it here, in the DLL code!
	
	Leave the job to the caller/calling application.
	
	Otherwise, as experienced, it might work for one case (command line program), but not for another (C#/WPF GUI application; .NET
	initializes COM itself -- in a seemingly incompatible way to this one)!

	See also <https://blogs.msdn.microsoft.com/larryosterman/2004/05/12/things-you-shouldnt-do-part-2-dlls-cant-ever-call-coinitialize-on-the-applications-thread/>. */
	
	hr = CoCreateInstance(CLSID_MbnInterfaceManager, NULL, CLSCTX_ALL, IID_IMbnInterfaceManager, (void**)&this->MbnInterfaceMgr);
	if (FAILED(hr))
		throw std::exception("Error in SIM constructor: CoCreateInstance() failed!");

	SAFEARRAY* sa;
	long lowerbound, upperbound;
	BSTR InterfaceID;

	hr = MbnInterfaceMgr->GetInterfaces(&sa);
	if (FAILED(hr))
		throw std::exception("Error in SIM constructor: GetInterfaces() failed!");

	SafeArrayGetLBound(sa, 1, &lowerbound);
	SafeArrayGetUBound(sa, 1, &upperbound);

	for (long l = lowerbound; l <= upperbound; l++)
	{
		SafeArrayGetElement(sa, &l, (void*)(&this->MbnInterface));
		this->MbnInterface->get_InterfaceID(&InterfaceID);
	}

	MbnInterfaceMgr->GetInterface(InterfaceID, &this->IMbnInterfaceObj);
	if (FAILED(hr))
		throw std::exception("Error in SIM constructor: GetInterface() failed!");

	SafeArrayDestroy(sa);

	// Register for event notifications.
	this->MbnInterfaceMgr->QueryInterface(IID_IConnectionPointContainer, (void**)&this->ConnPtCon);

	this->PinMgrEvt.AttachToSource(this->ConnPtCon); // Start listening for the PIN-Manager events.
	this->PinEvt.AttachToSource(this->ConnPtCon);    // Start listening for the PIN events.

	this->PinEvt.enter_event_handle = CreateEvent(NULL, FALSE, FALSE, NULL);
}


SIM::~SIM ()
{
	if (this->PinEvt.enter_event_handle)
	{
		CloseHandle(this->PinEvt.enter_event_handle);
		this->PinEvt.enter_event_handle = NULL;
	}
	
	this->PinEvt.DetachFromSource();    // Stop listening for the PIN events.
	this->PinMgrEvt.DetachFromSource(); // Stop listening for the PIN Manager events.
	
	this->MbnInterfaceMgr.Release();
}


/* -------------------------------------------------------------------------------------------------
Gathers data/information about the SIM and/or mobile broadband device.
------------------------------------------------------------------------------------------------- */

HRESULT SIM::getData ()
{
	// -------------------------------------------------------------------------------------------------
	IMbnSubscriberInformation* subscriber_information;

	MBN_PROVIDER home_provider;
	HRESULT hr = MbnInterface->GetHomeProvider(&home_provider);

	if (hr == S_OK)
		provider_name = home_provider.providerName;
	else
		provider_name = 0;
			
	hr = MbnInterface->GetSubscriberInformation(&subscriber_information);

	if (FAILED(hr))
	{
		return hr; 
	}
				
	MBN_INTERFACE_CAPS interface_capabilities;
	hr = MbnInterface->GetInterfaceCapability(&interface_capabilities);

	if (FAILED(hr))
	{
		return hr; 
	}

	IMEI = interface_capabilities.deviceID;	
	subscriber_information->get_SimIccID(&ICCID);
	subscriber_information->get_SubscriberID(&SubscriberID);

	/* Working with a SAFEARRAY is a bit strange.
	The main point to remember: The indices are in �reverse� order, compared to a normal C array.
		
	int array[7][8][9];               // C-style array index: [0][1][2]
		        |  |  |
		        |  |  +--- sa_bound[0]  // SAFEARRAY index.
		        |  +------ sa_bound[1]
		        +--------- sa_bound[2]

	"The right-most index is specified by rgsabound[0], the middle index by rgsabound[1] and the left most by rgsabound[2].
	This is not very intuitive but it is the way dimensional and bounds information is specified for a SafeArray."

	See https://limbioliong.wordpress.com/2011/06/22/passing-multi-dimensional-managed-array-to-c-part-2/ for more details.	*/

	SAFEARRAY* sa = NULL;
	SAFEARRAYBOUND sa_bounds[2];

	sa_bounds[0].lLbound = 0;
	sa_bounds[0].cElements = 2;
	sa_bounds[1].lLbound = 0;
	sa_bounds[1].cElements = max_telnum_count;

	sa = SafeArrayCreate(VT_BSTR, 2, sa_bounds);

	hr = subscriber_information->get_TelephoneNumbers(&sa);
		
	if (hr == S_OK)
	{
		long lbound, ubound;
		SafeArrayGetLBound(sa, 1, &lbound);
		SafeArrayGetUBound(sa, 1, &ubound);
		long dimY = ubound - lbound + 1;

		SafeArrayGetLBound(sa, 2, &lbound);
		SafeArrayGetUBound(sa, 2, &ubound);
		long dimX = ubound - lbound + 1;

		for (int i = 0; i < dimX; i++)
		{
			for (int j = 0; j < dimY; j++)
			{
				long indices [2];
				BSTR value;
				indices[0] = j;
				indices[1] = i;
				SafeArrayGetElement(sa, indices, &value);

				telephone_numbers[i] = value;
			}
		}
	}

	return hr;
}


/*--------------------------------------------------------------------------------------------------
Unlocks the SIM with the given PIN.
Throws on error.
Returns a status text on completion.
--------------------------------------------------------------------------------------------------*/

BSTR SIM::unlockSIM (char* PIN)
{	
	if (!PIN)
		throw std::invalid_argument("Missing parameter(s).");

	HRESULT hr;
	
	for (int i = 0; PIN[i] != 0; ++i)
	{
		if (!isdigit(PIN[i]))
		{
			throw std::invalid_argument("PIN is not a number.");
		}
	}
	
	IMbnPinManager* IMbnPinManagerObj;
	hr = IMbnInterfaceObj->QueryInterface(IID_IMbnPinManager, (void**) &IMbnPinManagerObj);

	IMbnPin* IMbnPinObj;
	hr = IMbnPinManagerObj->GetPin(MBN_PIN_TYPE_PIN1, &IMbnPinObj);

	switch (hr)
	{
		case S_OK:
			break;
	
		case E_PENDING:
			throw std::exception("Error: Pending.");

		case E_MBN_PIN_REQUIRED:
			throw std::exception("Error: PIN required.");
			
		case E_MBN_SIM_NOT_INSERTED:
			throw std::exception("Error: SIM not inserted.");

		case E_MBN_BAD_SIM:
			throw std::exception("Error: Bad SIM.");

		case ERROR_NOT_SUPPORTED:
			throw std::exception("Error: Not supported.");
	}

	MBN_PIN_MODE mbn_pin_mode;
	hr = IMbnPinObj->get_PinMode(&mbn_pin_mode);

	if (hr == S_OK)
	{
		switch (mbn_pin_mode)
		{
			case MBN_PIN_MODE_ENABLED:
				break;

			case MBN_PIN_MODE_DISABLED:
				break;
		}
	}

	// Convert char* to LPCWSTR.
	wchar_t* pin = new wchar_t [64];
    MultiByteToWideChar(CP_ACP, 0, PIN, -1, pin, 64);

	ULONG requestID;

	/* Regarding IMbnPin::Enter(): The HRESULT is irrelevant:
	"This is an asynchronous operation. If the method returns with success, then upon completion of the operation,
	the Mobile Broadband service will call the OnEnterComplete method of IMbnPinEvents."
	<https://msdn.microsoft.com/en-us/library/windows/desktop/dd323127%28v=vs.85%29.aspx> */
	
	IMbnPinObj->Enter(pin, &requestID);

	//IMbnPinManagerObj->GetPinState(&requestID);
	//Sleep(3000); // Give the device some miliseconds to react (1000 ms = 1 second). Tests showed this is necessary :-(

	/* Waiting for event notification that the request got completed.
	   The pure C++/command line program works well, even with infinite timeout.
 	   But the Managed-C++/C#/GUI program has some unsolved issues.
 	   Thus, we set a limit for the waiting time...
 	   ... and also ignore the timeout and handle like a success (if we got to here, it must have worked). :-/ */
	DWORD waitObject;
	waitObject = WaitForSingleObject(this->PinEvt.enter_event_handle, 10000);

	switch (waitObject)
	{
	case WAIT_OBJECT_0: // Event is triggerred for getting the status now.
	case WAIT_TIMEOUT:  // Fallthrough (see comments above).
		return ::SysAllocString(PinEvt.status_text);

	default:
		return ::SysAllocString(L"Default");
	}
}


/*--------------------------------------------------------------------------------------------------
Unblocks the SIM with the given PUK and sets the PIN to the provided value.
Throws on error.
Returns a status text on completion.
--------------------------------------------------------------------------------------------------*/

std::string SIM::unblockSIM (char* PUK, char* newPIN)
{
	if (!PUK || !newPIN)
		throw std::invalid_argument("Missing parameter(s).");
	
	HRESULT hr;
	
	for (int i = 0; PUK[i] != 0; ++i)
	{
		if (!isdigit(PUK[i]))
		{
			throw std::invalid_argument("Error: PUK is not a number.");
		}
	}

	for (int i = 0; newPIN[i] != 0; ++i)
	{
		if (!isdigit(newPIN[i]))
		{
			throw std::invalid_argument("Error: New PIN is not a number.");
		}
	}
	
	IMbnPinManager* IMbnPinManagerObj;
	hr = IMbnInterfaceObj->QueryInterface(IID_IMbnPinManager, (void**) &IMbnPinManagerObj);

	IMbnPin* IMbnPinObj;
	hr = IMbnPinManagerObj->GetPin(MBN_PIN_TYPE_PIN1, &IMbnPinObj);

	switch (hr)
	{
		case S_OK:
			break;

		case E_PENDING:
			throw std::exception("Error: Pending.");

		case E_MBN_PIN_REQUIRED:
			throw std::exception("Error: PIN required.");
			
		case E_MBN_SIM_NOT_INSERTED:
			throw std::exception("Error: SIM not inserted.");

		case E_MBN_BAD_SIM:
			throw std::exception("Error: Bad SIM.");

		case ERROR_NOT_SUPPORTED:
			throw std::exception("Error: Not supported.");
	}

	MBN_PIN_MODE mbn_pin_mode;
	hr = IMbnPinObj->get_PinMode(&mbn_pin_mode);

	if (hr == S_OK)
	{
		switch (mbn_pin_mode)
		{
			case MBN_PIN_MODE_ENABLED:
				break;

			case MBN_PIN_MODE_DISABLED:
				break;
		}
	}

	// Convert char* to LPCWSTR.
	wchar_t* puk = new wchar_t [64];
    MultiByteToWideChar(CP_ACP, 0, PUK, -1, puk, 64);

	wchar_t* newpin = new wchar_t [64];
    MultiByteToWideChar(CP_ACP, 0, newPIN, -1, newpin, 64);

	ULONG requestID;
	
	IMbnPinObj->Unblock(puk, newpin, &requestID);
	Sleep(3000); // Give the device some seconds to react (3000 ms = 3 seconds).

	IMbnPinManagerObj->GetPinState(&requestID);
	Sleep(3000); // Give the device some miliseconds to react (1000 ms = 1 second). Tests showed this is necessary :-(

	std::string result(PinMgrEvt.status_text);

	return result;
}


/*--------------------------------------------------------------------------------------------------
Changes the PIN of the SIM from the current value (curPIN) to the given new one (newPIN).
Throws on error.
Returns a status text on completion.
--------------------------------------------------------------------------------------------------*/

BSTR SIM::changePIN (char* curPIN, char* newPIN)
{
	if (!curPIN || !newPIN)
		throw std::invalid_argument("Missing parameter(s).");
	
	HRESULT hr;
	
	for (int i = 0; curPIN[i] != 0; ++i)
	{
		if (!isdigit(curPIN[i]))
		{
			throw std::invalid_argument("Error: PUK is not a number.");
		}
	}

	for (int i = 0; newPIN[i] != 0; ++i)
	{
		if (!isdigit(newPIN[i]))
		{
			throw std::invalid_argument("Error: New PIN is not a number.");
		}
	}
	
	IMbnPinManager* IMbnPinManagerObj;
	hr = IMbnInterfaceObj->QueryInterface(IID_IMbnPinManager, (void**) &IMbnPinManagerObj);

	IMbnPin* IMbnPinObj;
	hr = IMbnPinManagerObj->GetPin(MBN_PIN_TYPE_PIN1, &IMbnPinObj);

	switch (hr)
	{
		case S_OK:
			break;

		case E_PENDING:
			break;

		case E_MBN_PIN_REQUIRED:
			break;
			
		case E_MBN_SIM_NOT_INSERTED:
			throw std::exception("Error: SIM not inserted.");

		case E_MBN_BAD_SIM:
			throw std::exception("Error: Bad SIM.");

		case ERROR_NOT_SUPPORTED:
			throw std::exception("Error: Not supported.");
	}

	MBN_PIN_MODE mbn_pin_mode;
	hr = IMbnPinObj->get_PinMode(&mbn_pin_mode);

	if (hr == S_OK)
	{
		switch (mbn_pin_mode)
		{
			case MBN_PIN_MODE_ENABLED:
				break;

			case MBN_PIN_MODE_DISABLED:
				break;
		}
	}

	// Convert char* to LPCWSTR.
	wchar_t* curpin = new wchar_t [64];
    MultiByteToWideChar(CP_ACP, 0, curPIN, -1, curpin, 64);

	wchar_t* newpin = new wchar_t [64];
    MultiByteToWideChar(CP_ACP, 0, newPIN, -1, newpin, 64);

	ULONG requestID;

	IMbnPinObj->Change(curpin, newpin, &requestID);
	Sleep(3000); // Give the device some seconds to react (3000 ms = 3 seconds).

	return PinEvt.status_text;
}


/*--------------------------------------------------------------------------------------------------
Enables or disables the PIN authentication of the SIM.
Throws on error.
--------------------------------------------------------------------------------------------------*/

void SIM::enablePIN (bool enable, char* PIN)
{
	if (!PIN)
		throw std::invalid_argument("Missing parameter(s).");
	
	HRESULT hr;

	for (int i = 0; PIN[i] != 0; ++i)
	{
		if (!isdigit(PIN[i]))
		{
			throw std::invalid_argument("Error: PIN is not a number.");
		}
	}

	IMbnPinManager* IMbnPinManagerObj;
	hr = IMbnInterfaceObj->QueryInterface(IID_IMbnPinManager, (void**) &IMbnPinManagerObj);

	IMbnPin* IMbnPinObj;
	hr = IMbnPinManagerObj->GetPin(MBN_PIN_TYPE_PIN1, &IMbnPinObj);

	switch (hr)
	{
		case S_OK:
			break;

		case E_PENDING:
			break;

		case E_MBN_PIN_REQUIRED:
			break;
			
		case E_MBN_SIM_NOT_INSERTED:
			throw std::exception("Error: SIM not inserted.");

		case E_MBN_BAD_SIM:
			throw std::exception("Error: Bad SIM.");

		case ERROR_NOT_SUPPORTED:
			throw std::exception("Error: Not supported.");
	}

	MBN_PIN_MODE mbn_pin_mode;
	hr = IMbnPinObj->get_PinMode(&mbn_pin_mode);

	if (hr == S_OK)
	{
		switch (mbn_pin_mode)
		{
			case MBN_PIN_MODE_ENABLED:
				break;

			case MBN_PIN_MODE_DISABLED:
				break;
		}
	}

	// Convert char* to LPCWSTR.
	wchar_t* pin = new wchar_t [64];
    MultiByteToWideChar(CP_ACP, 0, PIN, -1, pin, 64);

	ULONG requestID;

	IMbnPinManagerObj->GetPinState(&requestID);

	if (enable == true)
		IMbnPinObj->Enable(pin, &requestID);
	else
		IMbnPinObj->Disable(pin, &requestID);

	Sleep(3000); // Give the device some seconds to react (3000 ms = 3 seconds).
}


/*--------------------------------------------------------------------------------------------------
Simple "getter" methodes; initally needed/used by the (C++)->(CLR/CLI)->(C#) methods.
--------------------------------------------------------------------------------------------------*/

BSTR SIM::getProviderName ()
{
	return provider_name;
}


BSTR SIM::getIMEI ()
{
	return IMEI;
}


BSTR SIM::getICCID ()
{
	return ICCID;
}


BSTR SIM::getSubscriberID ()
{
	return SubscriberID;
}


BSTR SIM::getTelNums ()
{
	BSTR separator  = ::SysAllocString(L";");
	BSTR terminator = ::SysAllocString(L"");
	BSTR value;
	
	for (int i = 0; i < sizeof(telephone_numbers)/sizeof(telephone_numbers); i++)
	{
		if (i+1 < sizeof(telephone_numbers) / sizeof(telephone_numbers))
			value = concatenateBSTR(telephone_numbers[i], separator);
		else
			value = concatenateBSTR(telephone_numbers[i], terminator);
	}

	return value;
}

} // Namespace.
