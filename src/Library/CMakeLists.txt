# ==================================================================================================
# Copyright © 2015-2018 Sascha Offe <so@saoe.net>.
# Published under the terms of the MIT license <http://opensource.org/licenses/MIT>.
# ==================================================================================================

project ("Library")

# --------------------------------------------------------------------------------------------------
# Shared/Dynamically linked (.dll) version of the library.

add_library (${PROJECT_NAME} SHARED)

# Rename the output name of the .DLL file.
string (TOLOWER ${APP_NAME} APP_NAME)
set_target_properties (${PROJECT_NAME} PROPERTIES OUTPUT_NAME ${APP_NAME})

target_sources (${PROJECT_NAME}
	PRIVATE
		eventhandler.cpp
		eventhandler.h
		mobilebroadbanddevice.h
		mobilebroadbanddevice.cpp
		sim.h
		sim.cpp
		../util.h
		../util.cpp
)

target_compile_definitions (${PROJECT_NAME}
	PRIVATE
		WIN32
		_WINDOWS
		_UNICODE
		UNICODE
		DLL_EXPORT # To set __declspec(dllexport) correctly in the header files.
)

target_include_directories (${PROJECT_NAME}
	PRIVATE
		${CMAKE_SOURCE_DIR}
)

target_link_libraries (${PROJECT_NAME}
	PRIVATE
		mbnapi_uuid.lib
)

set_target_properties (${PROJECT_NAME} PROPERTIES
	RUNTIME_OUTPUT_DIRECTORY ${SIMINFO_OUTPUT_DIR}
	RUNTIME_OUTPUT_DIRECTORY_DEBUG ${SIMINFO_OUTPUT_DIR}
	RUNTIME_OUTPUT_DIRECTORY_RELEASE ${SIMINFO_OUTPUT_DIR}
)
