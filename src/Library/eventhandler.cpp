/*==================================================================================================
Copyright � 2015-2018 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
==================================================================================================*/

/* At the moment, there are several separate classes for the different events.
But the basic code (constructor, QueryInterface, etc.) is similar (but not identical!).

Ideas:
a) Put it in a template class?
b) Or create one base class, with some case handling for the differences between events,
see https://github.com/Microsoft/Windows-classic-samples/blob/master/Samples/Win7Samples/netds/MB/mbapi/mbapi.cpp
Line 66+ and line 830+. */

#include <stdexcept>
#include <string>
#include <tchar.h>
#include <mbnapi.h>  // Mobile Broadband API; support begins with Windows 7.

#include "eventhandler.h"

namespace siminfo
{

////////////////////////////////////////////////////////////////////////////////////////////////////
// PinManagerEvents
////////////////////////////////////////////////////////////////////////////////////////////////////

HRESULT STDMETHODCALLTYPE PinManagerEvents::QueryInterface (REFIID riid, LPVOID* ppvObj)
{
	HRESULT hr = S_OK;
		
	if (ppvObj == NULL)
	{
		hr = E_INVALIDARG;
		return hr;
	}

	if ((riid == IID_IUnknown) || (riid == IID_IMbnPinManagerEvents))
	{
		AddRef();
		*ppvObj = this;
	}
	else
	{
		hr = E_NOINTERFACE;
	}
    
	return hr;
}


ULONG STDMETHODCALLTYPE PinManagerEvents::AddRef ()
{
	InterlockedIncrement((long*) &ref_count);
	return ref_count;
}


ULONG STDMETHODCALLTYPE PinManagerEvents::Release ()
{
	ULONG ret_val = ref_count - 1;

	if (InterlockedDecrement((long*) &ref_count) == 0)
	{
		delete this;
		return 0;
	}

	return ret_val;
}


/*--------------------------------------------------------------------------------------------------
Notification method called by the Mobile Broadband service to indicate the completion of an
asynchronous operation triggered by a call to the GetPinState method of IMbnPinManager.
--------------------------------------------------------------------------------------------------*/

HRESULT STDMETHODCALLTYPE PinManagerEvents::OnGetPinStateComplete (IMbnPinManager* PinManager, MBN_PIN_INFO PinInfo, ULONG RequestID, HRESULT Status)
{	
	switch (Status)
	{
		case S_OK:
			status_text = "Info: PIN state is OK.";
			break;

		case E_MBN_SIM_NOT_INSERTED:
			status_text = "Error: There is no SIM in the device.";
			break;

		case E_MBN_BAD_SIM:
			status_text = "Error: There is a bad SIM in the device.";
			break;

		case E_MBN_FAILURE:
		{
			switch (PinInfo.pinState)
			{
				case MBN_PIN_STATE_NONE: // Indicates that no PIN is currently required.			
					status_text = "Info: SIM is unlocked.";
					break;

				case MBN_PIN_STATE_ENTER: // Indicates that the device is currently locked and requires a PIN to be entered to unlock it.			
					status_text = "Error: SIM is locked! PIN required (" + std::to_string(PinInfo.attemptsRemaining) + " attempts remaining).";
					break;

				case MBN_PIN_STATE_UNBLOCK: // Indicates that the device is in a PIN blocked state and that the PIN needs to be unblocked using the corresponding PIN Unblock Key (PUK).
					status_text = "Error: SIM is blocked! PUK required (" + std::to_string(PinInfo.attemptsRemaining) + " attempts remaining).";
					break;

				default:
					status_text = "Status code (dec.): " + std::to_string(Status);
					break;
			}
		}

		default:
			status_text = "Info: Unknown status returned from OnGetPinStateComplete().";
			break;
	}
	return S_OK; // MSDN: "This method must return S_OK."
}


HRESULT STDMETHODCALLTYPE PinManagerEvents::OnPinListAvailable (IMbnPinManager* pinManager)
{
	return S_OK;
}


HRESULT PinManagerEvents::AttachToSource (IUnknown* Source)
{
	HRESULT hr = E_UNEXPECTED;
	IConnectionPointContainer* Container = 0;
	//IConnectionPoint*          ConnectionPoint = 0;
       
	hr = Source->QueryInterface(IID_IConnectionPointContainer, (void **)&Container);

	if (SUCCEEDED(hr))
	{
		hr = Container->FindConnectionPoint(IID_IMbnPinManagerEvents, &ConnectionPoint);
		
		if (SUCCEEDED(hr))
		{
			IUnknown* me = 0;
			hr = ConnectionPoint->QueryInterface(__uuidof(IUnknown),(void **)&me);
			hr = ConnectionPoint->Advise(this, &cookie); // Now that we know that the source fires our kind of events, we can attach to it.
		}

		Container->Release();
	}
	return hr;
}


HRESULT PinManagerEvents::DetachFromSource ()
{
	// [FIXME] Leads to a crash of the command line app.
	//HRESULT hr = ConnectionPoint->Unadvise(cookie);
	//ConnectionPoint->Release();
	//return hr;
	return S_OK;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// PinEvents
////////////////////////////////////////////////////////////////////////////////////////////////////

PinEvents::PinEvents ()
{
	status_text = ::SysAllocString(L"");
}


PinEvents::~PinEvents ()
{
	::SysFreeString(status_text);
}


HRESULT STDMETHODCALLTYPE PinEvents::QueryInterface (REFIID riid, LPVOID* ppvObj)
{
	HRESULT hr = S_OK;
		
	if (ppvObj == NULL)
	{
		hr = E_INVALIDARG;
		return hr;
	}

	if ((riid == IID_IUnknown) || (riid == IID_IMbnPinEvents))
	{
		AddRef();
		*ppvObj = this;
	}
	else
	{
		hr = E_NOINTERFACE;
	}
    
	return hr;
}


ULONG STDMETHODCALLTYPE PinEvents::AddRef ()
{
	InterlockedIncrement((long*) &ref_count);
	return ref_count;
}


ULONG STDMETHODCALLTYPE PinEvents::Release ()
{
	ULONG ret_val = ref_count - 1;

	if (InterlockedDecrement((long*) &ref_count) == 0)
	{
		delete this;
		return 0;
	}

	return ret_val;
}


/*--------------------------------------------------------------------------------------------------
Notification method called by the Mobile Broadband service to indicate that a PIN change operation
has completed.
--------------------------------------------------------------------------------------------------*/
		
HRESULT STDMETHODCALLTYPE PinEvents::OnChangeComplete (IMbnPin* Pin, MBN_PIN_INFO* PinInfo, ULONG RequestID, HRESULT Status)
{
	switch (Status)
	{
		case S_OK:
			::SysReAllocString(&status_text, L"OK: PIN change complete.");
			break;
		
		case E_MBN_PIN_REQUIRED:
			::SysReAllocString(&status_text, L"Error: A PIN is required for the operation to complete.");
			break;

		case E_MBN_SIM_NOT_INSERTED:
			::SysReAllocString(&status_text, L"Error: There is no SIM in the device.");
			break;

		case E_MBN_BAD_SIM:
			::SysReAllocString(&status_text, L"Error: There is a bad SIM in the device.");
			break;
		
		case E_MBN_PIN_DISABLED:
			::SysReAllocString(&status_text, L"Error: The PIN change operation is not supported for the disabled PIN.");
			break;
		
		case E_FAIL:
			::SysReAllocString(&status_text, L"Error: The operation could not be completed.");
			break;

		default:
			::SysReAllocString(&status_text, L"Info: Unknown status returned from OnChangeComplete().");
			break;
	}
	
	return S_OK; // MSDN: "This method must return S_OK."
}


HRESULT STDMETHODCALLTYPE PinEvents::OnDisableComplete (IMbnPin* Pin, MBN_PIN_INFO* PinInfo, ULONG RequestID, HRESULT Status)
{
	//_tprintf(_T("OnDisableComplete\n"));
	return S_OK;
}


HRESULT STDMETHODCALLTYPE PinEvents::OnEnableComplete (IMbnPin* Pin, MBN_PIN_INFO* PinInfo, ULONG RequestID, HRESULT Status)
{
	//_tprintf(_T("OnEnableComplete\n"));
	return S_OK;
}


HRESULT STDMETHODCALLTYPE PinEvents::OnEnterComplete (IMbnPin* Pin, MBN_PIN_INFO* PinInfo, ULONG RequestID, HRESULT Status)
{
	switch (Status)
	{
		case S_OK:
			::SysReAllocString(&status_text, L"OK: PIN entered successfully.");
			break;

		case E_MBN_PIN_REQUIRED:
			::SysReAllocString(&status_text, L"Error: A PIN is required for the operation to complete.");
			break;

		case E_MBN_SIM_NOT_INSERTED:
			::SysReAllocString(&status_text, L"Error: There is no SIM in the device.");
			break;

		case E_MBN_BAD_SIM:
			::SysReAllocString(&status_text, L"Error: There is a bad SIM in the device.");
			break;

		case E_FAIL:
			::SysReAllocString(&status_text, L"Error: The operation could not be completed.");
			break;
	
		case E_MBN_FAILURE:
		{
			switch (PinInfo->pinState)
			{
				case MBN_PIN_STATE_NONE: // Indicates that no PIN is currently required. 
					::SysReAllocString(&status_text, L"Info: SIM is unlocked.");
					break;

				case MBN_PIN_STATE_ENTER: // Indicates that the device is currently locked and requires a PIN to be entered to unlock it.
				{
					std::wstring x(L"Error: SIM is locked! PIN required (" + std::to_wstring(PinInfo->attemptsRemaining) + L" attempts remaining).");
					::SysReAllocString(&status_text, x.c_str());
					break;
				}

				case MBN_PIN_STATE_UNBLOCK:	// Indicates that the device is in a PIN blocked state and that the PIN needs to be unblocked using the corresponding PIN Unblock Key (PUK).
				{
					std::wstring y(L"Error: SIM is blocked! PUK required (" + std::to_wstring(PinInfo->attemptsRemaining) + L" attempts remaining).");
					::SysReAllocString(&status_text, y.c_str());
					break;
				}

				default:
					::SysReAllocString(&status_text, L"Default");
					break;
			}
			break;
		}

		default:
			::SysReAllocString(&status_text, L"Info: Unknown status returned from OnEnterComplete().");
			break;
	}

	return S_OK; // MSDN: "This method must return S_OK."
}


/*--------------------------------------------------------------------------------------------------
Notification method called by the Mobile Broadband service to indicate that a PIN unblock operation has completed.
--------------------------------------------------------------------------------------------------*/

HRESULT STDMETHODCALLTYPE PinEvents::OnUnblockComplete (IMbnPin* Pin, MBN_PIN_INFO* PinInfo, ULONG RequestID, HRESULT Status)
{
	switch (Status)
	{
		case S_OK:
			::SysReAllocString(&status_text, L"OK: SIM unblocked successfully.");
			break;

		case E_MBN_PIN_REQUIRED:
			::SysReAllocString(&status_text, L"Error: A PIN is required for the operation to complete.");
			break;

		case E_MBN_SIM_NOT_INSERTED:
			::SysReAllocString(&status_text, L"Error: There is no SIM in the device.");
			break;

		case E_MBN_BAD_SIM:
			::SysReAllocString(&status_text, L"Error: There is a bad SIM in the device.");
			break;

		case E_FAIL:
			::SysReAllocString(&status_text, L"Error: The operation could not be completed.");
			break;
	
		case E_MBN_FAILURE:
		{
			switch (PinInfo->pinState)
			{
				case MBN_PIN_STATE_NONE: // Indicates that no PIN is currently required. 
					::SysReAllocString(&status_text, L"Info: SIM is unlocked.");
					break;

				case MBN_PIN_STATE_ENTER: // Indicates that the device is currently locked and requires a PIN to be entered to unlock it.
				{
					std::wstring x(L"Error: SIM is locked! PIN required (" + std::to_wstring(PinInfo->attemptsRemaining) + L" attempts remaining).");
					::SysReAllocString(&status_text, x.c_str());
					break;
				}

				case MBN_PIN_STATE_UNBLOCK: // Indicates that the device is in a PIN blocked state and that the PIN needs to be unblocked using the corresponding PIN Unblock Key (PUK).
				{
					std::wstring y(L"Error: SIM is blocked! PUK required (" + std::to_wstring(PinInfo->attemptsRemaining) + L" attempts remaining).");
					::SysReAllocString(&status_text, y.c_str());
					break;
				}

				default:
					::SysReAllocString(&status_text, L"Default");
					break;
			}
			break;
		}

		default:
			::SysReAllocString(&status_text, L"Info: Unknown status returned from OnUnblockComplete().");
			break;
	}

	return S_OK; // MSDN: "This method must return S_OK."
}


HRESULT PinEvents::AttachToSource (IUnknown* Source)
{
	HRESULT hr = E_UNEXPECTED;
	IConnectionPointContainer* Container = 0;
	//IConnectionPoint*          ConnectionPoint = 0;
       
	hr = Source->QueryInterface(IID_IConnectionPointContainer, (void **)&Container);

	if (SUCCEEDED(hr))
	{
		hr = Container->FindConnectionPoint(IID_IMbnPinEvents, &ConnectionPoint);

		if (SUCCEEDED(hr))
		{
			IUnknown* sink;
			hr = ConnectionPoint->QueryInterface(IID_IUnknown,(void **)&sink);
			hr = ConnectionPoint->Advise(this, &cookie); // Now that we know that the source fires our kind of events, we can attach to it.
		}

		Container->Release();
	}
	return hr;
}


HRESULT PinEvents::DetachFromSource ()
{
	// [FIXME] Leads to a crash of the command line app.
	//HRESULT hr = ConnectionPoint->Unadvise(cookie);
	//ConnectionPoint->Release();
	//return hr;
	return S_OK;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// ConnectionEvents
////////////////////////////////////////////////////////////////////////////////////////////////////

ConnectionEvents::ConnectionEvents ()
{
	connect_status_text = ::SysAllocString(L"");
	disconnect_status_text = ::SysAllocString(L"");
}


ConnectionEvents::~ConnectionEvents ()
{
	::SysFreeString(disconnect_status_text);
	::SysFreeString(connect_status_text);
}


HRESULT STDMETHODCALLTYPE ConnectionEvents::QueryInterface (REFIID riid, LPVOID* ppvObj)
{
	HRESULT hr = S_OK;

	if (ppvObj == NULL)
	{
		hr = E_INVALIDARG;
		return hr;
	}

	if ((riid == IID_IUnknown) || (riid == IID_IMbnConnectionEvents))
	{
		AddRef();
		*ppvObj = this;
	}
	else
	{
		hr = E_NOINTERFACE;
	}

	return hr;
}


ULONG STDMETHODCALLTYPE ConnectionEvents::AddRef ()
{
	InterlockedIncrement((long*)&ref_count);
	return ref_count;
}


ULONG STDMETHODCALLTYPE ConnectionEvents::Release ()
{
	ULONG ret_val = ref_count - 1;

	if (InterlockedDecrement((long*)&ref_count) == 0)
	{
		delete this;
		return 0;
	}

	return ret_val;
}


/*--------------------------------------------------------------------------------------------------
Notification method called by the Mobile Broadband service to indicate that a PIN change operation
has completed.
--------------------------------------------------------------------------------------------------*/

HRESULT STDMETHODCALLTYPE ConnectionEvents::OnConnectComplete (IMbnConnection* newConnection, ULONG requestID, HRESULT status)
{
	switch (status)
	{
		case S_OK:
			::SysReAllocString(&connect_status_text, L"Connection established.");
			break;

		case E_MBN_SIM_NOT_INSERTED:
			::SysReAllocString(&connect_status_text, L"There is no SIM in the device.");
			break;

		case E_MBN_PIN_REQUIRED:
			::SysReAllocString(&connect_status_text, L"A PIN is required for the operation to complete.");
			break;

		case E_MBN_SERVICE_NOT_ACTIVATED:
			::SysReAllocString(&connect_status_text, L"The network service subscription has expired.");
			break;

		case E_MBN_PROVIDER_NOT_VISIBLE:
			::SysReAllocString(&connect_status_text, L"The provider is not visible.This applies only to manual registration mode.");
			break;

		case E_MBN_INVALID_ACCESS_STRING:
			::SysReAllocString(&connect_status_text, L"The connection access string is not correct.");
			break;

		case HRESULT_FROM_WIN32(ERROR_INVALID_PASSWORD):
			::SysReAllocString(&connect_status_text, L"The name or password using in the connection profile is not correct.");
			break;

		case E_MBN_VOICE_CALL_IN_PROGRESS:
			::SysReAllocString(&connect_status_text, L"An active voice call is in progress.");
			break;

		case E_MBN_MAX_ACTIVATED_CONTEXTS:
			::SysReAllocString(&connect_status_text, L"There is already an Mobile Broadband context active.The Mobile Broadband service does not currently support multiple active contexts.");
			break;

		case E_MBN_RADIO_POWER_OFF:
			::SysReAllocString(&connect_status_text, L"The device radio is off.");
			break;

		case E_MBN_PACKET_SVC_DETACHED:
			::SysReAllocString(&connect_status_text, L"No active attached packet service is available.");
			break;

		//case E_MBN_ACTIVE_CONNECTION: // MSVC: "... is undefined".
		//	::SysReAllocString(&status_text, L"The device is already connected to the network.");
		//	break;

		default:
			::SysReAllocString(&connect_status_text, L"Info: Unknown status returned from OnConnectComplete.");
			break;
	}

	SetEvent(connect_event_handle);

	return S_OK; // MSDN: "This method must return S_OK."
}


HRESULT STDMETHODCALLTYPE ConnectionEvents::OnDisconnectComplete (IMbnConnection* newConnection, ULONG requestID, HRESULT status)
{
	switch (status)
	{
		case S_OK:
			::SysReAllocString(&disconnect_status_text, L"Disconnect complete.");
			break;

		default:
			::SysReAllocString(&disconnect_status_text, L"Info: Unknown status returned from OnConnectComplete.");
			break;
	}

	SetEvent(disconnect_event_handle);
	
	return S_OK; // MSDN: "This method must return S_OK."
}


HRESULT STDMETHODCALLTYPE ConnectionEvents::OnConnectStateChange (IMbnConnection* newConnection)
{
	return S_OK; // MSDN: "This method must return S_OK."
}


HRESULT STDMETHODCALLTYPE ConnectionEvents::OnVoiceCallStateChange (IMbnConnection* newConnection)
{
	return S_OK; // MSDN: "This method must return S_OK."
}


HRESULT ConnectionEvents::AttachToSource (IUnknown* Source)
{
	HRESULT hr = E_UNEXPECTED;
	IConnectionPointContainer* Container = 0;
	//IConnectionPoint*          ConnectionPoint = 0;

	hr = Source->QueryInterface(IID_IConnectionPointContainer, (void **)&Container);

	if (SUCCEEDED(hr))
	{
		hr = Container->FindConnectionPoint(IID_IMbnConnectionEvents, &ConnectionPoint);
		
		if (SUCCEEDED(hr))
		{
			IUnknown* sink;
			hr = ConnectionPoint->QueryInterface(IID_IUnknown, (void **)&sink);
			hr = ConnectionPoint->Advise(this, &cookie); // Now that we know that the source fires our kind of events, we can attach to it.

		}

		Container->Release();
	}

	return hr;
}


HRESULT ConnectionEvents::DetachFromSource ()
{
	HRESULT hr = ConnectionPoint->Unadvise(cookie);
	ConnectionPoint->Release();
	return hr;
}

} // Namespace.
