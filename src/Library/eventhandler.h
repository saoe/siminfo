/*==================================================================================================
Copyright � 2015-2018 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
==================================================================================================*/

#ifndef SIMINFO_EVENTHANDLER_H
#define SIMINFO_EVENTHANDLER_H

#include <string>
#include <tchar.h>
#include <mbnapi.h>  // Mobile Broadband API; support begins with Windows 7.

#ifdef DLL_EXPORT
	#define DLL_API __declspec(dllexport)
#else
	#define DLL_API __declspec(dllimport)
#endif

/* Disabling Warning C4251, so that it won't clutter the build output (I'm currently the only creator and consumer of the affected DLL anyway).
Some details : http://www.unknownroad.com/rtfm/VisualStudio/warningC4251.html */
#pragma warning(push)
#pragma warning(disable:4251)

namespace siminfo
{

/* -----------------------------------------------------------------------------
Class to register and handle PIN manager events, based on COM connection points.
http://www.techrepublic.com/article/add-events-to-your-application-with-com-connection-points/

Implements the IMbnPinManagerEvents interface.
----------------------------------------------------------------------------- */

class DLL_API PinManagerEvents : public IMbnPinManagerEvents
{
	public:
		std::string status_text;
		
		HRESULT STDMETHODCALLTYPE QueryInterface (REFIID riid, LPVOID* ppvObj);
		ULONG   STDMETHODCALLTYPE AddRef ();
		ULONG   STDMETHODCALLTYPE Release ();
		
		HRESULT STDMETHODCALLTYPE OnGetPinStateComplete (IMbnPinManager* PinManager, MBN_PIN_INFO PinInfo, ULONG RequestID, HRESULT Status);
		HRESULT STDMETHODCALLTYPE OnPinListAvailable (IMbnPinManager* pinManager);

		HRESULT AttachToSource (IUnknown* Source);
		HRESULT DetachFromSource ();

	private:
		DWORD             cookie;
		IConnectionPoint* ConnectionPoint;
		ULONG             ref_count;
};


/* -----------------------------------------------------------------------------
Class to register and handle PIN events, based on COM connection points.
http://www.techrepublic.com/article/add-events-to-your-application-with-com-connection-points/

This is the sink that gets attached to the source of the PIN events and acts whenever an PIN event is triggered.
Implements the IMbnPinEvents interface.
----------------------------------------------------------------------------- */

class DLL_API PinEvents : public IMbnPinEvents
{
	public:
		BSTR status_text;
		HANDLE enter_event_handle;

		PinEvents ();
		~PinEvents ();
		
		HRESULT STDMETHODCALLTYPE QueryInterface (REFIID riid, LPVOID* ppvObj);
		ULONG   STDMETHODCALLTYPE AddRef ();
		ULONG   STDMETHODCALLTYPE Release ();
		
		HRESULT STDMETHODCALLTYPE OnChangeComplete (IMbnPin* Pin, MBN_PIN_INFO* PinInfo, ULONG RequestID, HRESULT Status);
		HRESULT STDMETHODCALLTYPE OnDisableComplete (IMbnPin* Pin, MBN_PIN_INFO* PinInfo, ULONG RequestID, HRESULT Status);
		HRESULT STDMETHODCALLTYPE OnEnableComplete (IMbnPin* Pin, MBN_PIN_INFO* PinInfo, ULONG RequestID, HRESULT Status);
		HRESULT STDMETHODCALLTYPE OnEnterComplete (IMbnPin* Pin, MBN_PIN_INFO* PinInfo, ULONG RequestID, HRESULT Status);
		HRESULT STDMETHODCALLTYPE OnUnblockComplete (IMbnPin* Pin, MBN_PIN_INFO* PinInfo, ULONG RequestID, HRESULT Status);

		HRESULT AttachToSource (IUnknown* Source);
		HRESULT DetachFromSource ();

	private:
		DWORD             cookie;          // The cookie given to us by Advise
		IConnectionPoint* ConnectionPoint; // The interface where we called Advise.
		ULONG             ref_count;
};


/* -----------------------------------------------------------------------------
Class to register and handle connection events, based on COM connection points.
----------------------------------------------------------------------------- */

class DLL_API ConnectionEvents : public IMbnConnectionEvents
{
	public:
		BSTR   connect_status_text, disconnect_status_text;
		HANDLE connect_event_handle, disconnect_event_handle;

		ConnectionEvents ();
		~ConnectionEvents ();

		HRESULT STDMETHODCALLTYPE QueryInterface (REFIID riid, LPVOID* ppvObj);
		ULONG   STDMETHODCALLTYPE AddRef ();
		ULONG   STDMETHODCALLTYPE Release ();

		HRESULT STDMETHODCALLTYPE OnConnectComplete (IMbnConnection* newConnection, ULONG requestID, HRESULT status);
		HRESULT STDMETHODCALLTYPE OnDisconnectComplete (IMbnConnection* newConnection, ULONG requestID, HRESULT status);
		HRESULT STDMETHODCALLTYPE OnConnectStateChange (IMbnConnection* newConnection);
		HRESULT STDMETHODCALLTYPE OnVoiceCallStateChange (IMbnConnection* newConnection);

		HRESULT AttachToSource (IUnknown* Source);
		HRESULT DetachFromSource ();

	private:
		DWORD             cookie;
		IConnectionPoint* ConnectionPoint;
		ULONG             ref_count;
};

} // Namespace.

#pragma warning(pop)

#endif