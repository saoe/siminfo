/*==================================================================================================
Copyright � 2015-2018 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
==================================================================================================*/

#ifndef SIMINFO_SIM_H
#define SIMINFO_SIM_H

#include <atlbase.h>
#include <string>
#include <mbnapi.h>  // Mobile Broadband API; support begins with Windows 7.
#include <tchar.h>
#include "eventhandler.h"

#ifdef DLL_EXPORT
	#define DLL_API __declspec(dllexport)
#else
	#define DLL_API __declspec(dllimport)
#endif

/* Disabling Warning C4251, so that it won't clutter the build output (I'm currently the only creator and consumer of the affected DLL anyway).
Some details : http://www.unknownroad.com/rtfm/VisualStudio/warningC4251.html */
#pragma warning(push)
#pragma warning(disable:4251)

namespace siminfo
{

class DLL_API SIM
{
	public:
		bool PIN_query;
		static const int max_telnum_count = 10; // I just assume that there won't be more 10 telephone nummbers associated with a single SIM card.
		BSTR telephone_numbers[max_telnum_count] = { 0 };

		BSTR getProviderName ();
		BSTR getIMEI ();
		BSTR getICCID ();
		BSTR getSubscriberID ();
		BSTR getTelNums ();
		
		SIM ();
		~SIM ();

		HRESULT     getData ();
		BSTR        unlockSIM (char* PIN);
		std::string unblockSIM (char* PUK, char* newPIN);
		BSTR        changePIN (char* curPIN, char* newPIN);
		void        enablePIN (bool enable, char* PIN);

	private:
		BSTR provider_name;
		BSTR IMEI;
		BSTR ICCID;
		BSTR SubscriberID; // = GSM IMSI or CDMA MIN/IRM. Can usually only be accessed when the SIM is unlocked.

		CComPtr<IMbnInterfaceManager> MbnInterfaceMgr;
		CComPtr<IMbnInterface> MbnInterface;
		CComPtr<IMbnInterface> IMbnInterfaceObj;
		CComPtr<IConnectionPointContainer> ConnPtCon;

		PinManagerEvents PinMgrEvt;
		PinEvents PinEvt;
};

} // Namespace.

#pragma warning(pop)

#endif