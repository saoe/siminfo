/*==================================================================================================
Copyright � 2015-2018 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
==================================================================================================*/

#include <math.h>
#include <atlbase.h>
#include "eventhandler.h"
#include "mobilebroadbanddevice.h"
#include "util.h"

namespace siminfo
{

MobileBroadbandDevice::MobileBroadbandDevice ()
{
	HRESULT hr;

	/* Note: You must initialize the COM library on a thread before you call any of the library functions (like CoCreateInstance(),
	but do not do it here, in the DLL code!

	Leave the job to the caller/calling application.

	Otherwise, as experienced, it might work for one case (command line program), but not for another (C#/WPF GUI application; .NET
	initializes COM itself -- in a seemingly incompatible way to this one)!

	See also <https://blogs.msdn.microsoft.com/larryosterman/2004/05/12/things-you-shouldnt-do-part-2-dlls-cant-ever-call-coinitialize-on-the-applications-thread/>. */

	hr = CoCreateInstance(CLSID_MbnInterfaceManager, NULL, CLSCTX_ALL, IID_IMbnInterfaceManager, (void**)&this->MbnInterfaceMgr);

	if (FAILED(hr))
		return;

	hr = CoCreateInstance(CLSID_MbnConnectionManager, NULL, CLSCTX_ALL, IID_IMbnConnectionManager, (void**)&this->MbnConnectionMgr);
	if (FAILED(hr))
		throw std::exception("Error in MobileBroadbandDevice constructor: CoCreateInstance() failed!");

	SAFEARRAY *sa, *sa2;
	long lowerbound, upperbound;
	long lowerbound2, upperbound2;
	BSTR InterfaceID, ConnectionID;

	hr = MbnInterfaceMgr->GetInterfaces(&sa);
	if (FAILED(hr))
		throw std::exception("Error in MobileBroadbandDevice constructor: GetInterfaces() failed!");

	hr = MbnConnectionMgr->GetConnections(&sa2);
	if (FAILED(hr))
		throw std::exception("Error in MobileBroadbandDevice constructor: GetConnections() failed!");

	SafeArrayGetLBound(sa, 1, &lowerbound);
	SafeArrayGetUBound(sa, 1, &upperbound);

	for (long l = lowerbound; l <= upperbound; l++)
	{
		SafeArrayGetElement(sa, &l, (void*)(&this->MbnInterface));
		this->MbnInterface->get_InterfaceID(&InterfaceID);
	}

	hr = MbnInterfaceMgr->GetInterface(InterfaceID, &this->IMbnInterfaceObj);
	if (FAILED(hr))
		throw std::exception("Error in MobileBroadbandDevice constructor: GetInterface() failed!");

	SafeArrayDestroy(sa);

	// ---

	SafeArrayGetLBound(sa2, 1, &lowerbound2);
	SafeArrayGetUBound(sa2, 1, &upperbound2);

	for (long l = lowerbound2; l <= upperbound2; l++)
	{
		SafeArrayGetElement(sa2, &l, (void*)(&this->MbnConnection));
		this->MbnConnection->get_InterfaceID(&ConnectionID);
	}

	hr = this->MbnConnectionMgr->GetConnection(ConnectionID, &this->MbnConnectionObj);
	if (FAILED(hr))
		throw std::exception("Error in MobileBroadbandDevice constructor: GetConnection() failed!");
	
	SafeArrayDestroy(sa2);

	// Register for event notifications.
	this->MbnConnectionMgr->QueryInterface(IID_IConnectionPointContainer, (void**)&this->ConnPtCon);
	this->ConnEvt.AttachToSource(this->ConnPtCon); // Start listening for the connection events.

	this->ConnEvt.connect_event_handle = CreateEvent(NULL, FALSE, FALSE, NULL);
	this->ConnEvt.disconnect_event_handle = CreateEvent(NULL, FALSE, FALSE, NULL);
}


MobileBroadbandDevice::~MobileBroadbandDevice ()
{	
	if (this->ConnEvt.connect_event_handle)
	{
		CloseHandle(this->ConnEvt.connect_event_handle);
		this->ConnEvt.connect_event_handle = NULL;
	}

	if (this->ConnEvt.disconnect_event_handle)
	{
		CloseHandle(this->ConnEvt.disconnect_event_handle);
		this->ConnEvt.disconnect_event_handle = NULL;
	}

	this->ConnEvt.DetachFromSource(); // Stop listening for the connection events.	
	this->MbnInterfaceMgr.Release();
}


/* -------------------------------------------------------------------------------------------------
Set some default values for the (initial) profile.
------------------------------------------------------------------------------------------------- */

void MobileBroadbandDevice::prepareProfileWithDefaultValues (SIM sim_card)
{
	new_connection_profile.Name                  = ::SysAllocString(L"New Profile");
	new_connection_profile.IsDefault             = ::SysAllocString(L"true");
	new_connection_profile.ProfileCreationType   = ::SysAllocString(L"UserProvisioned");
	new_connection_profile.SubscriberID          = ::SysAllocString(sim_card.getSubscriberID());
	new_connection_profile.SimIccID              = ::SysAllocString(sim_card.getICCID());
	new_connection_profile.AutoConnectOnInternet = ::SysAllocString(L"false");
	new_connection_profile.HomeProviderName      = ::SysAllocString(new_connection_profile.Name);
	new_connection_profile.ConnectionMode        = ::SysAllocString(L"manual");
	new_connection_profile.AccessString          = ::SysAllocString(L"APN");
	new_connection_profile.UserName              = ::SysAllocString(L"username");
	new_connection_profile.Password              = ::SysAllocString(L"password");
	new_connection_profile.Compression           = ::SysAllocString(L"DISABLE");
	new_connection_profile.AuthProtocol          = ::SysAllocString(L"PAP");
}


/* -------------------------------------------------------------------------------------------------
Gathers data/information about the SIM and/or mobile broadband device.
------------------------------------------------------------------------------------------------- */

HRESULT MobileBroadbandDevice::getData ()
{
	HRESULT hr;

	// Get network connectivity staus of a device.
	IMbnConnection* connection;
	MBN_ACTIVATION_STATE mbn_activation_state;

	hr = MbnInterface->GetConnection(&connection);

	if (hr == S_OK)
		connection->GetConnectionState(&mbn_activation_state, NULL);
	else
		return hr;

	ProfileXmlData = getProfileData();
	getCelluarClass();
	getDataClass();
	getCurrentDataClass();
	getSupportedDataClasses();

	return hr;
}


/* -------------------------------------------------------------------------------------------------
Establishes a connection based on the XML profile string (which it queries here).
------------------------------------------------------------------------------------------------- */

BSTR MobileBroadbandDevice::connect ()
{
	this->connection_id = 0;

	if (0 < getSignalStrength())
	{
		// Establishes a connection based on the XML profile string.
		HRESULT connection_result = this->MbnConnectionObj->Connect(MBN_CONNECTION_MODE_TMP_PROFILE, getProfileData(), &this->connection_id);

		switch (connection_result)
		{
			case S_OK:
				break;

			case E_HANDLE:
				break;

			case E_INVALIDARG:
				break;

			case E_MBN_MAX_ACTIVATED_CONTEXTS:
				break;

			case __HRESULT_FROM_WIN32(ERROR_SERVICE_NOT_ACTIVE):
				break;

			case __HRESULT_FROM_WIN32(ERROR_NOT_FOUND):
				break;

			default:
				break;
		}

		/* Waiting for event notification that the request got completed.
		   The pure C++/command line program works well, even with infinite timeout.
		   But the Managed-C++/C#/GUI program has some unsolved issues.
		   Thus, we set a limit for the waiting time...	
		   ... and also ignore the timeout and handle like a success (if we got to here, it must have worked). :-/ */
		DWORD waitObject;
		waitObject = WaitForSingleObject(this->ConnEvt.connect_event_handle, 10000);

		switch (waitObject)
		{
			case WAIT_OBJECT_0: // Event is triggerred for getting the status now.
			case WAIT_TIMEOUT:  // Fallthrough (see comments above).
				return ::SysAllocString(ConnEvt.connect_status_text);

			default:
				return ::SysAllocString(L"Default");
		}
	}
	else
	{
		throw std::exception("Error: No signal detected.");
	}
}


/* -------------------------------------------------------------------------------------------------
Disconnects the context identified by the member variable "connection_id".
------------------------------------------------------------------------------------------------- */

BSTR MobileBroadbandDevice::disconnect ()
{
	this->connection_id = 0;

	HRESULT disconnection_result = this->MbnConnectionObj->Disconnect(&this->connection_id);

	switch (disconnection_result)
	{
		case S_OK:
			break;

		case __HRESULT_FROM_WIN32(ERROR_NOT_FOUND):
			break;

		default:
			break;
	}

	/* Waiting for event notification that the request got completed.
	   The pure C++/command line program works well, even with infinite timeout.
	   But the Managed-C++/C#/GUI program has some unsolved issues.
	   Thus, we set a limit for the waiting time...
	   ... and also ignore the timeout and handle like a success (if we got to here, it must have worked). :-/ */
	DWORD waitObject;
	waitObject = WaitForSingleObject(this->ConnEvt.disconnect_event_handle, 10000);

	switch (waitObject)
	{
		case WAIT_OBJECT_0: // Event is triggerred for getting the status now.
		case WAIT_TIMEOUT:  // Fallthrough (see comments above).
			return ::SysAllocString(ConnEvt.disconnect_status_text);

		default:
			return ::SysAllocString(L"Default");
	}
}


/* -------------------------------------------------------------------------------------------------
Checks the readiness of a mobile broadband device to engage in cellular network traffic operations.
------------------------------------------------------------------------------------------------- */

bool MobileBroadbandDevice::isDeviceReady (/*output parameter*/ std::string& status_text)
{
	bool status = false;
	
	if (this->MbnInterfaceMgr == nullptr)
	{
		status_text = "Error: No mobile broadband interface found.";
		return false; 
	}

	// The MBN_READY_STATE enumerated type contains values that indicate the readiness of a Mobile Broadband device to engage in cellular network traffic operations.
	MBN_READY_STATE mbn_ready_state;

	this->MbnInterface->GetReadyState(&mbn_ready_state);

	switch (mbn_ready_state)
	{
		case MBN_READY_STATE_OFF:
			status_text = "The mobile broadband device stack is off.";
			status = false;
			break;

		case MBN_READY_STATE_INITIALIZED:
			status_text = "The card and stack is powered up and ready to be used for mobile broadband operations.";
			status = true;
			break;

		case MBN_READY_STATE_SIM_NOT_INSERTED:
			status_text = "The SIM is not inserted.";
			status = false;
			break;

		case MBN_READY_STATE_BAD_SIM:
			status_text = "The SIM is invalid (PIN Unblock Key retrials have exceeded the limit).";
			status = false;
			break;

		case MBN_READY_STATE_FAILURE:
			status_text = "General device failure.";
			status = false;
			break;

		case MBN_READY_STATE_NOT_ACTIVATED:
			status_text = "The subscription is not activated.";
			status = false;
			break;

		case MBN_READY_STATE_DEVICE_LOCKED:
			status_text = "The device is locked by a PIN or password which is preventing the device from initializing and registering onto the network.";
			status = false;
			break;

		case MBN_READY_STATE_DEVICE_BLOCKED:
			status_text = "The device is blocked by a PUK or password which is preventing the device from initializing and registering onto the network.";
			status = false;
			break;
			
		default:
			break;
	}

	return status;
}


/* -------------------------------------------------------------------------------------------------
Checks if the mobile broadband device is blocked (requires a PUK).
------------------------------------------------------------------------------------------------- */

bool MobileBroadbandDevice::isDeviceBlocked ()
{
	bool status = false;

	if (this->MbnInterfaceMgr == nullptr)
	{
		throw std::runtime_error("Error: No mobile broadband interface found.");
		//return false;
	}

	// The MBN_READY_STATE enumerated type contains values that indicate the readiness of a Mobile Broadband device to engage in cellular network traffic operations.
	MBN_READY_STATE mbn_ready_state;

	this->MbnInterface->GetReadyState(&mbn_ready_state);

	switch (mbn_ready_state)
	{
		case MBN_READY_STATE_DEVICE_BLOCKED:
			status = true;
			break;

		default:
			status = false;
			break;
	}

	return status;
}


/* -------------------------------------------------------------------------------------------------
Reports the signal strength received by the Mobile Broadband device (see note below!).
Converts to an approximated percent value.

1) GetSignalStrength(): For GSM based devices it reports signal strength as signal strength received in a coded value.
   For CDMA devices it reports based on the Compensated RSSI (accounts for noise) and not based on Raw RSSI.

   Signal Strength (in dBm) | Coded Value (Min: 0 Max: 31)
   -------------------------+-----------------------------
               -113 or less | 0
                        ... | ...
             -51 or greater | 31
    Unknown or undetectable | MBN_RSSI_UNKNOWN
 
2) To get an approx. percent value, multiply with 3.125 (because 100% / 32 = 3.125).
3) Round down to the floor() value.

But note:
In a test case, comparing it with the output of "netsh mbn show interface", the value matches: Both methods show 38-40%.
But the graphical representation on Windows 10 shows 4 out of 5 bars at the same time -- that would be ca. 80% (HSPA).
Maybe <https://stackoverflow.com/a/19046489> explains the difference, and MS is using another method in the GUI...?
	>
	> You probably aren't doing anything wrong, Microsoft is though.
	> 
	> Your connection is 3G you say, but the Microsoft scale is based on GSM (2G).
	> GSM gives a connection down to -113 dBm, but UMTS gives a connection down to -120 dBm.
	> A different parameter is being measured as well, which makes the Microsoft implementation even less valid.
	> 
	> If we believe the Microsoft scale, your values of 1 - 4 are about -111 to -105 dBm.
	> 
	> In UMTS, this is quite a respectable signal.

------------------------------------------------------------------------------------------------- */

int MobileBroadbandDevice::getSignalStrength ()
{
	IMbnSignal* signal;
	this->MbnInterface->QueryInterface(IID_IMbnSignal, (void**) &signal);
	
	unsigned long signal_strength;
	signal->GetSignalStrength(&signal_strength);

	if (signal_strength != MBN_RSSI_UNKNOWN) // When the signal strength is not known or it is not detectable by the device then this is set to MBN_RSSI_UNKNOWN.
		return static_cast<int>(floor(signal_strength * 3.125));
	else
		return 0;
}


/* -------------------------------------------------------------------------------------------------
Returns the cellular technology used by the device.
Private member; fills the internal variable.
-------------------------------------------------------------------------------------------------- */

void MobileBroadbandDevice::getCelluarClass ()
{
	this->MbnInterface->GetInterfaceCapability(&interface_capabilities);
	cellular_class = interface_capabilities.cellularClass;
}


/* -------------------------------------------------------------------------------------------------
Returns the cellular technology used by the device.
Public member; for displaying a user-friendly translation from numeric value to text value.
-------------------------------------------------------------------------------------------------- */

BSTR MobileBroadbandDevice::getCelluarClassName ()
{
	switch (this->cellular_class)
	{
		case MBN_CELLULAR_CLASS_GSM:
			return ::SysAllocString(L"GSM");
		
		case MBN_CELLULAR_CLASS_CDMA:
			return ::SysAllocString(L"CDMA");
		
		case MBN_CELLULAR_CLASS_NONE:
			return ::SysAllocString(L"None");
		
		default:
			return ::SysAllocString(L"(Unknown Celluar Class)");
	}
}


/* -------------------------------------------------------------------------------------------------
Get the data class in the current network.
------------------------------------------------------------------------------------------------ */

void MobileBroadbandDevice::getDataClass ()
{
	this->MbnInterface->GetInterfaceCapability(&interface_capabilities);
	data_classes = interface_capabilities.dataClass;
}


/* -------------------------------------------------------------------------------------------------
Gets the current data class in the current network.
Public member; for displaying a user-friendly translation from numeric value to text value.
------------------------------------------------------------------------------------------------ */

void MobileBroadbandDevice::getCurrentDataClass ()
{
	IMbnRegistration* registration;
	this->MbnInterface->QueryInterface(IID_IMbnRegistration, (void**) &registration);
	
	unsigned long dc;
	registration->GetCurrentDataClass(&dc);
	this->data_class = (MBN_DATA_CLASS)dc;
}


/* -------------------------------------------------------------------------------------------------
Gets the current data class in the current network.
Public member; for displaying a user-friendly translation from numeric value to text value.
------------------------------------------------------------------------------------------------ */

BSTR MobileBroadbandDevice::getCurrentDataClassName ()
{
	switch (this->data_class)
	{
		case MBN_DATA_CLASS_NONE:        // No data class.
			return ::SysAllocString(L"(No Data Class detected)");
		
		case MBN_DATA_CLASS_GPRS:        // The GPRS data class implemented by GSM providers.
			return ::SysAllocString(L"GPRS");
	
		case MBN_DATA_CLASS_EDGE:        // The EDGE data class implemented by GSM providers.
			return ::SysAllocString(L"EDGE");
		
		case MBN_DATA_CLASS_UMTS:        // The UMTS data class implemented by mobile radio providers.
			return ::SysAllocString(L"UMTS");
		
		case MBN_DATA_CLASS_HSDPA:       // The HSDPA data class implemented by mobile radio providers.
			return ::SysAllocString(L"HSDPA");
		
		case MBN_DATA_CLASS_LTE:         // The LTE data class implemented by mobile radio providers.
			return ::SysAllocString(L"LTE");
	
		case MBN_DATA_CLASS_HSUPA:       // The HSUPA (High Speed Uplink Packet Access) data class.
			return ::SysAllocString(L"HSUPA");
	
		case MBN_DATA_CLASS_1XRTT:       // The 1xRTT data class implemented by CDMA providers.
			return ::SysAllocString(L"1xRTT");
	
		case MBN_DATA_CLASS_1XEVDO:	     // The IxEV-DO data class implemented by CDMA providers.
			return ::SysAllocString(L"1xEV-DO");
	
		case MBN_DATA_CLASS_1XEVDO_REVA: // The IxEV-DO RevA data class implemented by CDMA providers.
			return ::SysAllocString(L"1xEV-DO RevA");
	
		case MBN_DATA_CLASS_1XEVDV:      // The 1xXEV-DV data class.
			return ::SysAllocString(L"1xXEV-DV");
	
		case MBN_DATA_CLASS_3XRTT:       // The 3xRTT data class.
			return ::SysAllocString(L"3xRTT");
	
		case MBN_DATA_CLASS_1XEVDO_REVB: // The 1xEV-DO RevB data class, which is defined for future use.
			return ::SysAllocString(L"1xEV-DO RevB");
	
		case MBN_DATA_CLASS_UMB:         // The UMB data class.
			return ::SysAllocString(L"UMB");
	
		case MBN_DATA_CLASS_CUSTOM:      // The custom data class.
			return ::SysAllocString(L"(Custom Data Class)");
	
		default:
			return ::SysAllocString(L"(Unknown Data Class)");
	}
}


/* -------------------------------------------------------------------------------------------------
Returns the custom data class in the current network.
------------------------------------------------------------------------------------------------ */

BSTR MobileBroadbandDevice::getCustomDataClass ()
{
	if (SysStringLen(this->interface_capabilities.customDataClass) == 0)
		return ::SysAllocString(L"(Not Available)");
	else	
		return this->interface_capabilities.customDataClass;
}


/* -------------------------------------------------------------------------------------------------
(WORK IN PROGRESS) Returns the supported data classes in the current network.
------------------------------------------------------------------------------------------------ */

BSTR MobileBroadbandDevice::getSupportedDataClasses ()
{
	BSTR result = ::SysAllocString(L"");
	
	if (cellular_class == MBN_CELLULAR_CLASS_GSM)
	{
		if (data_classes & MBN_DATA_CLASS_GPRS)
			result = concatenateBSTR(result, ::SysAllocString(L"GPRS, "));
		if (data_classes & MBN_DATA_CLASS_EDGE)
			result = concatenateBSTR(result, ::SysAllocString(L"EDGE, "));
		if (data_classes & MBN_DATA_CLASS_UMTS)
			result = concatenateBSTR(result, ::SysAllocString(L"UMTS, "));
		if (data_classes & MBN_DATA_CLASS_HSDPA)
			result = concatenateBSTR(result, ::SysAllocString(L"HSDPA, "));
		if (data_classes & MBN_DATA_CLASS_HSUPA)
			result = concatenateBSTR(result, ::SysAllocString(L"HSUPA, "));
		if (data_classes & MBN_DATA_CLASS_LTE)
			result = concatenateBSTR(result, ::SysAllocString(L"LTE, "));
	}
	else if (cellular_class == MBN_CELLULAR_CLASS_CDMA)
	{
		if (data_classes & MBN_DATA_CLASS_1XRTT)
			result = concatenateBSTR(result, ::SysAllocString(L"1xRTT, "));
		if (data_classes & MBN_DATA_CLASS_1XEVDO)
			result = concatenateBSTR(result, ::SysAllocString(L"1xEV-DO, "));
		if (data_classes & MBN_DATA_CLASS_1XEVDO_REVA)
			result = concatenateBSTR(result, ::SysAllocString(L"1xEV-DO (RevA), "));
		if (data_classes & MBN_DATA_CLASS_1XEVDV)
			result = concatenateBSTR(result, ::SysAllocString(L"1xXEV-DV, "));
		if (data_classes & MBN_DATA_CLASS_3XRTT)
			result = concatenateBSTR(result, ::SysAllocString(L"3xRTT, "));
		if (data_classes & MBN_DATA_CLASS_1XEVDO_REVB)
			result = concatenateBSTR(result, ::SysAllocString(L"1xEV-DO (RevB), "));
		if (data_classes & MBN_DATA_CLASS_UMB)
			result = concatenateBSTR(result, ::SysAllocString(L"UMB, "));
	}
	// ... and finally, adding also the custom class, if available:
	if (data_classes & MBN_DATA_CLASS_CUSTOM)
		result = concatenateBSTR(result, getCustomDataClass());

	// Not-so-elegant(?) way of removing a trailing comma.
	std::string tmp = convertBSTRToMBS(result);
	tmp.erase(tmp.find_last_not_of(", ") + 1);
	result = charToBSTR(const_cast<char*>(tmp.c_str()));

	return result;
}


/* -------------------------------------------------------------------------------------------------
Returns the name of the device manufacturer.
------------------------------------------------------------------------------------------------ */

BSTR MobileBroadbandDevice::getManufacturer ()
{
	if (SysStringLen(this->interface_capabilities.manufacturer) == 0)
		return ::SysAllocString(L"(Not Available)");
	else
		return this->interface_capabilities.manufacturer;
}


/* -------------------------------------------------------------------------------------------------
Returns the device model.
------------------------------------------------------------------------------------------------ */

BSTR MobileBroadbandDevice::getModel ()
{
	if (SysStringLen(this->interface_capabilities.model) == 0)
		return ::SysAllocString(L"(Not Available)");
	else
		return this->interface_capabilities.model;
}


/* -------------------------------------------------------------------------------------------------
Returns the firmware-specific information for this device.
------------------------------------------------------------------------------------------------ */

BSTR MobileBroadbandDevice::getFirmwareInfo ()
{
	if (SysStringLen(this->interface_capabilities.firmwareInfo) == 0)
		return ::SysAllocString(L"(Not Available)");
	else
		return this->interface_capabilities.firmwareInfo;
}


/* -------------------------------------------------------------------------------------------------
Returns the current mobile broadband profile as a string in XML format, as described in the
Mobile Broadband Profile Schema <https://msdn.microsoft.com/en-us/library/windows/desktop/dd323300.aspx>
------------------------------------------------------------------------------------------------- */

BSTR MobileBroadbandDevice::getProfileData ()
{
	CComPtr<IMbnConnectionProfileManager> connection_profile_manager = NULL;
	CoCreateInstance(CLSID_MbnConnectionProfileManager, NULL, CLSCTX_ALL, IID_IMbnConnectionProfileManager, (void**)&connection_profile_manager);
		
	SAFEARRAY* profile_data_sa = NULL;
	long       profile_data_lLower;
	long       profile_data_lUpper;
	HRESULT    hr;
	BSTR       profile_data;
	
	hr = connection_profile_manager->GetConnectionProfiles(this->MbnInterface, &profile_data_sa);

	if (hr == S_OK)
	{
		SafeArrayGetLBound(profile_data_sa, 1, &profile_data_lLower);
		SafeArrayGetUBound(profile_data_sa, 1, &profile_data_lUpper);

		IMbnConnectionProfile* connection_profile = NULL;
		
		for (long l = profile_data_lLower; l <= profile_data_lUpper; l++)
		{
			hr = SafeArrayGetElement(profile_data_sa, &l, (void*)(&connection_profile));
		}

		if (connection_profile != NULL)
			connection_profile->GetProfileXmlData(&profile_data);
		else
			profile_data = ::SysAllocString(L"(No profile data found)");

		SafeArrayDestroy(profile_data_sa); // Is this OK to do here?
	}
	else
	{
		profile_data = ::SysAllocString(L"(No profile data found)");
	}

	return profile_data;
}


/* -------------------------------------------------------------------------------------------------
Deletes the (currently active) profile(s) and creates a new one.

Returns 'true' on success or 'false' on failure.

The method IMbnConnectionProfile::UpdateProfile() didn't work while testing; I also read some posts
on forums that described the same behavior and my previous (scripting) attempt also only functioned
when the profile was deleted and then re-created.
------------------------------------------------------------------------------------------------- */

bool MobileBroadbandDevice::makeNewProfile ()
{
	BSTR new_xml;

	// ---------------------------------------------------
	// Composing the new connection profile as XML string.
	
	BSTR tag_xmlheader = ::SysAllocString(L"<?xml version=\"1.0\"?>\n");
	BSTR tag_mbnprofile_start = ::SysAllocString(L"<MBNProfile xmlns=\"http://www.microsoft.com/networking/WWAN/profile/v1\">\n");

	new_xml = siminfo::concatenateBSTR(tag_xmlheader, tag_mbnprofile_start);

	// ---

	BSTR tag_name_start = ::SysAllocString(L"\t<Name>");
	BSTR tag_name_value = ::SysAllocString(new_connection_profile.Name);
	BSTR tag_name_end = ::SysAllocString(L"</Name>\n");

	new_xml = siminfo::concatenateBSTR(new_xml, tag_name_start);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_name_value);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_name_end);

	::SysFreeString(tag_xmlheader);
	::SysFreeString(tag_name_start);
	::SysFreeString(tag_name_value);
	::SysFreeString(tag_name_end);

	// ---

	BSTR tag_isdefault_start = ::SysAllocString(L"\t<IsDefault>");
	BSTR tag_isdefault_value = ::SysAllocString(new_connection_profile.IsDefault);
	BSTR tag_isdefault_end = ::SysAllocString(L"</IsDefault>\n");

	new_xml = siminfo::concatenateBSTR(new_xml, tag_isdefault_start);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_isdefault_value);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_isdefault_end);

	::SysFreeString(tag_isdefault_start);
	::SysFreeString(tag_isdefault_value);
	::SysFreeString(tag_isdefault_end);

	// ---
	
	BSTR tag_profilecreationtype_start = ::SysAllocString(L"\t<ProfileCreationType>");
	BSTR tag_profilecreationtype_value = ::SysAllocString(new_connection_profile.ProfileCreationType);
	BSTR tag_profilecreationtype_end = ::SysAllocString(L"</ProfileCreationType>\n");

	new_xml = siminfo::concatenateBSTR(new_xml, tag_profilecreationtype_start);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_profilecreationtype_value);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_profilecreationtype_end);

	::SysFreeString(tag_profilecreationtype_start);
	::SysFreeString(tag_profilecreationtype_value);
	::SysFreeString(tag_profilecreationtype_end);

	// ---

	BSTR tag_subscriberid_start = ::SysAllocString(L"\t<SubscriberID>");
	BSTR tag_subscriberid_value = ::SysAllocString(new_connection_profile.SubscriberID);
	BSTR tag_subscriberid_end = ::SysAllocString(L"</SubscriberID>\n");

	new_xml = siminfo::concatenateBSTR(new_xml, tag_subscriberid_start);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_subscriberid_value);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_subscriberid_end);

	::SysFreeString(tag_subscriberid_start);
	::SysFreeString(tag_subscriberid_value);
	::SysFreeString(tag_subscriberid_end);

	// ---

	BSTR tag_simiccid_start = ::SysAllocString(L"\t<SimIccID>");
	BSTR tag_simiccid_value = ::SysAllocString(new_connection_profile.SimIccID);
	BSTR tag_simiccid_end = ::SysAllocString(L"</SimIccID>\n");

	new_xml = siminfo::concatenateBSTR(new_xml, tag_simiccid_start);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_simiccid_value);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_simiccid_end);

	::SysFreeString(tag_simiccid_start);
	::SysFreeString(tag_simiccid_value);
	::SysFreeString(tag_simiccid_end);

	// ---

	BSTR tag_homeprovidername_start = ::SysAllocString(L"\t<HomeProviderName>");
	BSTR tag_homeprovidername_value = ::SysAllocString(new_connection_profile.HomeProviderName);
	BSTR tag_homeprovidername_end = ::SysAllocString(L"</HomeProviderName>\n");

	new_xml = siminfo::concatenateBSTR(new_xml, tag_homeprovidername_start);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_homeprovidername_value);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_homeprovidername_end);
	
	::SysFreeString(tag_homeprovidername_start);
	::SysFreeString(tag_homeprovidername_value);
	::SysFreeString(tag_homeprovidername_end);

	// ---

	BSTR tag_autoconnectoninternet_start = ::SysAllocString(L"\t<AutoConnectOnInternet>");
	BSTR tag_autoconnectoninternet_value = ::SysAllocString(new_connection_profile.AutoConnectOnInternet);
	BSTR tag_autoconnectoninternet_end = ::SysAllocString(L"</AutoConnectOnInternet>\n");

	new_xml = siminfo::concatenateBSTR(new_xml, tag_autoconnectoninternet_start);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_autoconnectoninternet_value);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_autoconnectoninternet_end);
	
	::SysFreeString(tag_autoconnectoninternet_start);
	::SysFreeString(tag_autoconnectoninternet_value);
	::SysFreeString(tag_autoconnectoninternet_end);

	// ---

	BSTR tag_connectionmode_start = ::SysAllocString(L"\t<ConnectionMode>");
	BSTR tag_connectionmode_value = ::SysAllocString(new_connection_profile.ConnectionMode);
	BSTR tag_connectionmode_end = ::SysAllocString(L"</ConnectionMode>\n");

	new_xml = siminfo::concatenateBSTR(new_xml, tag_connectionmode_start);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_connectionmode_value);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_connectionmode_end);

	::SysFreeString(tag_connectionmode_start);
	::SysFreeString(tag_connectionmode_value);
	::SysFreeString(tag_connectionmode_end);

	// ---

	BSTR tag_context_start = ::SysAllocString(L"\t<Context>\n");

	new_xml = siminfo::concatenateBSTR(new_xml, tag_context_start);

		BSTR tag_accessstring_start = ::SysAllocString(L"\t\t<AccessString>");
		BSTR tag_accessstring_value = ::SysAllocString(new_connection_profile.AccessString);
		BSTR tag_accessstring_end = ::SysAllocString(L"</AccessString>\n");

		new_xml = siminfo::concatenateBSTR(new_xml, tag_accessstring_start);
		new_xml = siminfo::concatenateBSTR(new_xml, tag_accessstring_value);
		new_xml = siminfo::concatenateBSTR(new_xml, tag_accessstring_end);

		::SysFreeString(tag_accessstring_start);
		::SysFreeString(tag_accessstring_value);
		::SysFreeString(tag_accessstring_end);

		// ---

		BSTR tag_userlogoncred_start = ::SysAllocString(L"\t\t<UserLogonCred>\n");

		new_xml = siminfo::concatenateBSTR(new_xml, tag_userlogoncred_start);

			// ---
	
			BSTR tag_username_start = ::SysAllocString(L"\t\t\t<UserName>");
			BSTR tag_username_value = ::SysAllocString(new_connection_profile.UserName);
			BSTR tag_username_end = ::SysAllocString(L"</UserName>\n");

			new_xml = siminfo::concatenateBSTR(new_xml, tag_username_start);
			new_xml = siminfo::concatenateBSTR(new_xml, tag_username_value);
			new_xml = siminfo::concatenateBSTR(new_xml, tag_username_end);

			::SysFreeString(tag_username_start);
			::SysFreeString(tag_username_value);
			::SysFreeString(tag_username_end);

			// ---
	
			BSTR tag_password_start = ::SysAllocString(L"\t\t\t<Password>");
			BSTR tag_password_value = ::SysAllocString(new_connection_profile.Password);
			BSTR tag_password_end = ::SysAllocString(L"</Password>\n");

			new_xml = siminfo::concatenateBSTR(new_xml, tag_password_start);
			new_xml = siminfo::concatenateBSTR(new_xml, tag_password_value);
			new_xml = siminfo::concatenateBSTR(new_xml, tag_password_end);

			::SysFreeString(tag_password_start);
			::SysFreeString(tag_password_value);
			::SysFreeString(tag_password_end);

			// ---
	
		BSTR tag_userlogoncred_end = ::SysAllocString(L"\t\t</UserLogonCred>\n");

		new_xml = siminfo::concatenateBSTR(new_xml, tag_userlogoncred_end);

		// ---

		BSTR tag_compression_start = ::SysAllocString(L"\t\t<Compression>");
		BSTR tag_compression_value = ::SysAllocString(new_connection_profile.Compression);
		BSTR tag_compression_end = ::SysAllocString(L"</Compression>\n");

		new_xml = siminfo::concatenateBSTR(new_xml, tag_compression_start);
		new_xml = siminfo::concatenateBSTR(new_xml, tag_compression_value);
		new_xml = siminfo::concatenateBSTR(new_xml, tag_compression_end);

		::SysFreeString(tag_compression_start);
		::SysFreeString(tag_compression_value);
		::SysFreeString(tag_compression_end);

		// ---
	
		BSTR tag_authprotocol_start = ::SysAllocString(L"\t\t<AuthProtocol>");
		BSTR tag_authprotocol_value = ::SysAllocString(new_connection_profile.AuthProtocol);
		BSTR tag_authprotocol_end = ::SysAllocString(L"</AuthProtocol>\n");

		new_xml = siminfo::concatenateBSTR(new_xml, tag_authprotocol_start);
		new_xml = siminfo::concatenateBSTR(new_xml, tag_authprotocol_value);
		new_xml = siminfo::concatenateBSTR(new_xml, tag_authprotocol_end);

		::SysFreeString(tag_authprotocol_start);
		::SysFreeString(tag_authprotocol_value);
		::SysFreeString(tag_authprotocol_end);

	// ---

	BSTR tag_context_end = ::SysAllocString(L"\t</Context>\n");
	BSTR tag_mbnprofile_end = ::SysAllocString(L"</MBNProfile>");

	new_xml = siminfo::concatenateBSTR(new_xml, tag_context_end);
	new_xml = siminfo::concatenateBSTR(new_xml, tag_mbnprofile_end);

	::SysFreeString(tag_userlogoncred_start);
	::SysFreeString(tag_userlogoncred_end);
	::SysFreeString(tag_context_end);
	::SysFreeString(tag_mbnprofile_end);

	// -----------------------------------------------------------------
	// Using the IMbnInterface to get access to the connection profiles.

	CComPtr<IMbnConnectionProfileManager> connection_profile_manager = NULL;
	CoCreateInstance(CLSID_MbnConnectionProfileManager, NULL, CLSCTX_ALL, IID_IMbnConnectionProfileManager, (void**)&connection_profile_manager);
		
	SAFEARRAY* profile_data_sa = NULL;
	long profile_data_lLower;
	long profile_data_lUpper;
	BSTR profile_data = ::SysAllocString(L"(No profile data found)");
	IMbnConnectionProfile* connection_profile = NULL;

	HRESULT hr = connection_profile_manager->GetConnectionProfiles(this->MbnInterface, &profile_data_sa);

	if (hr == S_OK)
	{
		// Delete existing profile(s) and (re)create a new one.
		SafeArrayGetLBound(profile_data_sa, 1, &profile_data_lLower);
		SafeArrayGetUBound(profile_data_sa, 1, &profile_data_lUpper);
	
		for (long l = profile_data_lLower; l <= profile_data_lUpper; l++)
		{
			SafeArrayGetElement(profile_data_sa, &l, (void*)(&connection_profile));
		
			if (connection_profile != NULL)
				connection_profile->Delete();
		}
	
		connection_profile_manager->CreateConnectionProfile(new_xml);

		SafeArrayDestroy(profile_data_sa); // Is this OK to do here?
	}
	else
	{
		// Create an initial profile.
		connection_profile_manager->CreateConnectionProfile(new_xml);
	}

	::SysFreeString(new_xml);

	return true;
}

} // Namespace.
