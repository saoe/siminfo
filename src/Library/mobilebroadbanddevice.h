/* =================================================================================================
Copyright � 2015-2018 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see License.txt).
================================================================================================= */

#ifndef SIMINFO_MOBILEBROADBANDDEVICE_H
#define SIMINFO_MOBILEBROADBANDDEVICE_H

//#include <atlbase.h>
#include "eventhandler.h"
#include <tchar.h>
#include <mbnapi.h>  // Mobile Broadband API; support begins with Windows 7.
#include "SIM.h"

#ifdef DLL_EXPORT
	#define DLL_API __declspec(dllexport)
#else
	#define DLL_API __declspec(dllimport)
#endif

/* Disabling Warning C4251, so that it won't clutter the build output (I'm currently the only creator and consumer of the affected DLL anyway).
Some details : http://www.unknownroad.com/rtfm/VisualStudio/warningC4251.html */
#pragma warning(push)
#pragma warning(disable:4251)

namespace siminfo
{

class DLL_API MobileBroadbandDevice
{
	public:
		BSTR ProfileXmlData;

		struct New_Connection_Profile
		{
			BSTR Name;
			BSTR IsDefault;
			BSTR ProfileCreationType;
			BSTR SubscriberID;
			BSTR SimIccID;
			BSTR HomeProviderName; // From MSDN: "This element is optional and is set by the Mobile Broadband service for internal use. An application should not set this field while creating or updating a profile."
			BSTR AutoConnectOnInternet;
			BSTR ConnectionMode;
			BSTR AccessString;
			BSTR UserName;
			BSTR Password;
			BSTR Compression;
			BSTR AuthProtocol;
		} new_connection_profile;

		MobileBroadbandDevice ();
		~MobileBroadbandDevice ();

		HRESULT getData ();
		void    prepareProfileWithDefaultValues (SIM sim_card);
		bool    makeNewProfile ();
		bool    isDeviceReady (/*output parameter*/ std::string& status_text);
		bool    isDeviceBlocked ();
		BSTR    getCelluarClassName ();
		BSTR    getCurrentDataClassName ();
		BSTR    getCustomDataClass ();
		BSTR    getSupportedDataClasses ();
		BSTR    getManufacturer ();
		BSTR    getModel ();
		BSTR    getFirmwareInfo ();
		int     getSignalStrength ();
		BSTR    connect ();
		BSTR    disconnect ();

	private:
		ULONG              connection_id;
		ULONG              data_classes;
		MBN_DATA_CLASS     data_class;
		MBN_INTERFACE_CAPS interface_capabilities;
		MBN_CELLULAR_CLASS cellular_class;

		CComPtr<IMbnInterfaceManager>      MbnInterfaceMgr;
		CComPtr<IMbnInterface>             MbnInterface;
		CComPtr<IMbnInterface>             IMbnInterfaceObj;
		CComPtr<IMbnConnectionManager>     MbnConnectionMgr;
		CComPtr<IMbnConnection>            MbnConnection;
		CComPtr<IMbnConnection>            MbnConnectionObj;
		CComPtr<IConnectionPointContainer> ConnPtCon;

		ConnectionEvents ConnEvt;

		BSTR  getProfileData ();
		void  getCelluarClass ();
		void  getDataClass ();
		void  getCurrentDataClass ();
};

} // Namespace.

#pragma warning(pop)

#endif
