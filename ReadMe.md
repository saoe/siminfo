# SIMInfo

Command-line utility program for Microsoft Windows to display miscellaneous data from a SIM card (such as the ICCID/IMEI/IMSI, telephone number, connection profile, etc.) and execute several actions (like unlocking the SIM, creating a connection profile, connecting to the WWAN, and more...).

Uses the Mobile Broadband API and thus requires Windows 7 or later.  
Tested under Windows 7 and Windows 10.

You can [**download**](https://bitbucket.org/saoe/siminfo/downloads/) a binary relase of the current version or try to build it yourself (see below).


## Usage

Run `siminfo.exe [/option]` from a command prompt.

| Option                         | Description                                          |
|--------------------------------|------------------------------------------------------|
| /All                           | Display all information at once. |
| /ICCID                         | Display only the SIM card's [Integrated Circuit Card Identifier](http://en.wikipedia.org/wiki/Subscriber_identity_module#ICCID). |
| /IMEI                          | Display only the SIM card's [International Mobile Station Equipment Identity](http://en.wikipedia.org/wiki/International_Mobile_Station_Equipment_Identity). |
| /IMSI                          | Display only the SIM card's [International Mobile Subscriber Identity](http://en.wikipedia.org/wiki/International_mobile_subscriber_identity). |
| /TelNum                        | Display the telephone number(s) associated with the SIM card. |
| /Signal                        | Display the approximated signal strength received by the device (in percent). |
| /DataClass                     | Display the data class currently used by the device. |
| /Profile                       | Display the connection profile data (in the XML format of the [Mobile Broadband Profile Schema v1](https://msdn.microsoft.com/en-us/library/windows/desktop/dd323300.aspx)). |
| /Connect                       | Connect to the WWAN, using the default profile. |
| /Disconnect                    | Disconnect from the WWAN. |
| /UnlockSIM _PIN_               | Unlock the SIM card with the provided _PIN_. |
| /UnblockSIM _PUK_ _NewPIN_     | Unblock the SIM card with the provided _PUK_ and set the PIN to the value of _NewPIN_. |
| /ChangePIN _CurPIN_ _NewPIN_   | Change the PIN of the SIM card. |
| /EnablePIN _PIN_               | Enable the PIN authentication of the SIM. |
| /DisablePIN _PIN_              | Disable the PIN authentication of the SIM. |
| /UpdateProfile [_Key_=_Value_] | Without any K=V option: Creates a new/initial profile (according to the [Mobile Broadband Profile Schema v1 specification](https://msdn.microsoft.com/en-us/library/windows/desktop/dd323300.aspx)) with default values. Otherwise, the given key value will explicitly be set. Program’s return code on failure is 1. See the next table for details. |


### UpdateProfile _Key_=_Value_

Not all keys must be provided, but the default values might not be working with your provider.

**Important:** The exact upper/lower case of each value does matter (e.g. _AuthProtocol=MsCHAPv2_), except for plain text values (_text_). Otherwise the new profile might not be created at all!

| Key                   | Valid values                                                                            |
|-----------------------|-----------------------------------------------------------------------------------------|
| Name                  | _text_                                                                                  |
| IsDefault             | `true` or `false`                                                                       |
| ProfileCreationType   | `UserProvisioned` or `AdminProvisioned` or `OperatorProvisioned` or `DeviceProvisioned` |
| AutoConnectOnInternet | `true` or `false`                                                                       |
| ConnectionMode        | `manual` or `auto` or `auto-home`                                                       |
| AccessString          | _text_                                                                                  |
| UserName              | _text_                                                                                  |
| Password              | _text_                                                                                  |
| Compression           | `ENABLE` or `DISABLE`                                                                   |
| AuthProtocol          | `NONE` or `PAP` or `CHAP` or `MsCHAPv2`                                                 |


## Build

See comments in [`CMakeLists.txt`](src/CMakeLists.txt) for additional information on how to build the project.

Requirements:

* Files from the [repository](https://bitbucket.org/saoe/siminfo)
* [CMake](http://www.cmake.org>) (3.8.2 or higher)
* Microsoft Visual Studio 2017
* Microsoft Windows 10 SDK

--------------------------------------------------------------------------------

## Known issues or bugs

1. Does not follow the [Mobile Broadband API Best Practices](https://docs.microsoft.com/en-us/windows/desktop/mbn/mobile-broadband-best-practices)
in several places. Currently known violations:
_Do Not Cache Functional Objects_: I create them only once, in the class constructor.
_Using The Pin Unblock API requires administrator or NCO privileges_: I'm developing and testing with an admin account, thus I did not stumble upon it yet (or check for it in the code).

2. On a brand-new system: When no profile (ever) existed, "/all" returns totally empty handed.
When creating a profile and then deleting it again via NetSH (see below), then at least the SIM data (ICCID, etc.) and signal strength will be displayed.
`netsh> mbn delete profile interface="Mobile Broadband Connection" name="myProfileName"`
Use "mbn show interfaces" to get the correct (translated!) name.
See in hidden folder C:\ProgramData\Microsoft\WwanSvc\Profiles, should be empty.

3. The signal strength reported by IMbnSignal::GetSignalStrength() and 'netsh mbn show interface'
don't really match with the bars displayed in the GUI frontend. Reason might be a problem with
Microsofts measurement scale for newer standards than GSM (see comment at MobileBroadbandDevice::getSignalStrength() for more details).

4. (Library/eventhandler.cpp, 2018-11-26) Had to comment most of PinEvents::DetachFromSource() and
PinManagerEvents::DetachFromSource() to prevent an AppCrash when the destructor of class SIM ran.
Strangely enough, crash happend only for the command line app, and also: ConnectionEvents::DetachFromSource()
could remain untouched. The latter is the same code, but for class MobileBroadbandDevice.
Anyway, need to overhaul the event code anyways...

5. GUI: The graphical frontend is not fully developed or tested!


## Ideas and random notes

- isLocked: PIN, PUK, ...; maybe parts of isDeviceReady() can be helpful: MBN_READY_STATE_DEVICE_LOCKED/IMbnInterface::GetReadyState method.

--------------------------------------------------------------------------------

Copyright © 2015-2019 [Sascha Offe](http://www.saoe.net/).  
Published under the [MIT license](https://opensource.org/licenses/MIT) (follow the link or look in to the source code's [License.txt](https://bitbucket.org/saoe/siminfo/tip/License.txt) for details).

